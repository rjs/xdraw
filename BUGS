Known bugs in xdraw
===================

I know about these bugs, so you probably don't need to tell me about
them, unless you have something more to report about them (or perhaps
a fix...).

 * WriteFileFromPixmap and WriteFileFromWindow dump core for some
   reason!
 * ReportFocusChangeEvents doesn't work properly, and key events are
   not reported when the mouse cursor is outside the xdraw window.
   This seems to be a feature in the X Toolkit's event handling model
   (events seem to be sent to the window with the mouse cursor, if it
   has keyboard focus), and I haven't found a way to change it. Please
   tell me if you know of a way to fix for this...
 * The parser requires whitespace around each parenthesis in a list.
   Fixing this isn't very difficult, but would complicate the parser
   somewhat.
 * DrawJustifiedString works correctly only when drawing a single line
   of left-to-right text.
 * The -useviewport feature is somewhat experimental, and should be
   more configurable (see the xdraw(1x) manual page for details).

There are a few other minor bugs and suggestions for improvement
marked with XXX in the source code files.


Reporting a bug
---------------

If you wish to report a bug, you can e-mail me at rjs@netti.fi. Please
include the version of xdraw you used and a detailed description of
the bug. If you can, try a newer version of xdraw first: the bug might
already be fixed there.

It is much easier for me to fix the bug if you can tell me a way to
reproduce it simply and reliably (for example, a short list of xdraw
commands that demonstrate the bug when executed).

If you have an idea on where to find the bug, or even a fix for it,
feel free to tell me about it...
