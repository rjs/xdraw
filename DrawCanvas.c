/*
 * DrawCanvas -- A widget that accepts drawing commands
 * Copyright (C) 1998-2001 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

/*
 * DrawCanvas.c -- Main widget code
 */

#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <math.h>
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/Xmu/Drawing.h>
#include <X11/xpm.h>
#include "DrawCanvasP.h"
#include "version.h"

/* Default width and height of the widget */
#define DEF_WIDTH 200
#define DEF_HEIGHT 200

/* Maximum length of an event string */
#define EVENT_STR_SIZE 512

#define ro(field) XtOffsetOf(DrawCanvasRec, draw.field)
static XtResource resources[] = {
  { XtNallowFileRead, XtCAllowFileAccess, XtRBoolean, sizeof(Boolean),
    ro(allow_file_read), XtRImmediate, (XtPointer) True },
  { XtNallowFileWrite, XtCAllowFileAccess, XtRBoolean, sizeof(Boolean),
    ro(allow_file_write), XtRImmediate, (XtPointer) True },
  { XtNcallback, XtCCallback, XtRCallback, sizeof(XtCallbackList),
    ro(callback), XtRImmediate, (XtPointer) NULL },
  { XtNforeground, XtCForeground, XtRPixel, sizeof(Pixel),
    ro(foreground), XtRString, XtDefaultForeground }
};
#undef ro

static void Initialize(Widget request, Widget new,
  ArgList args, Cardinal *num_args);
static void Redisplay(Widget w, XEvent *event, Region region);
static void Resize(Widget w);
static Boolean SetValues(Widget current, Widget request, Widget new,
  ArgList args, Cardinal *num_args);
static XtGeometryResult QueryGeometry(Widget w,
  XtWidgetGeometry *request, XtWidgetGeometry *reply);
static void Destroy(Widget w);

char *DrawCanvasCommand(Widget w, const char *command);

static void start_parse(DrawCanvasWidget ww, const char *command);
static void *parse_malloc(DrawCanvasWidget ww, int size);
static int parse_count_whitespace(DrawCanvasWidget ww, const char *pos);
static void parse_whitespace(DrawCanvasWidget ww);
static int parse_token_end(DrawCanvasWidget ww, char *endpos, int zero);
static int parse_symbol(DrawCanvasWidget ww, char **value);
static int parse_int(DrawCanvasWidget ww, int *value);
static int parse_string(DrawCanvasWidget ww, char **value);
static int parse_special_char(DrawCanvasWidget ww, char ch);
static int parse_list_start(DrawCanvasWidget ww);
static int parse_list_next_element(DrawCanvasWidget ww);
static int parse_list_end(DrawCanvasWidget ww);
static int parse_list_count_elements(DrawCanvasWidget ww, int *value);
static int parse_command_end(DrawCanvasWidget ww);
static void parse_error(DrawCanvasWidget ww, int showpos, char *msg);
static void end_parse(DrawCanvasWidget ww);

static void returnf(DrawCanvasWidget ww, char *fmt, ...);
static void eventf(DrawCanvasWidget ww, char *fmt, ...);
static void change_event_mask(DrawCanvasWidget ww,
  EventMask add, EventMask remove);
static void event_handler(Widget w, XtPointer closure, XEvent *event,
  Boolean *continue_to_dispatch);
static char *statestr(unsigned int state);

static void init_colors(DrawCanvasWidget ww);
static unsigned long get_color(DrawCanvasWidget ww, const char *name);
static void free_colors(DrawCanvasWidget ww);
static void deinit_colors(DrawCanvasWidget ww);

static void init_fonts(DrawCanvasWidget ww);
static Font get_font(DrawCanvasWidget ww, const char *name);
static void free_fonts(DrawCanvasWidget ww);
static void deinit_fonts(DrawCanvasWidget ww);

static void init_gcs(DrawCanvasWidget ww);
static void free_dgc(DrawCanvasWidget ww);
static void new_dgc(DrawCanvasWidget ww);
static void gcstack_push_and_reset(DrawCanvasWidget ww);
static void gcstack_pop(DrawCanvasWidget ww);
static void deinit_gcs(DrawCanvasWidget ww);

static void init_winpm(DrawCanvasWidget ww);
static void clear_winpm(DrawCanvasWidget ww);
static void resize_winpm(DrawCanvasWidget ww, int width, int height);
static void deinit_winpm(DrawCanvasWidget ww);

static void init_pixmaps(DrawCanvasWidget ww);
static int new_pixmap(DrawCanvasWidget ww, const char *name,
  int width, int height);
static int add_pixmap(DrawCanvasWidget ww, const char *name,
  Pixmap pixmap, Pixmap shapemask);
static int find_pixmap(DrawCanvasWidget ww, const char *name,
  Pixmap *pixmap_return, Pixmap *shapemask_return);
static int free_pixmap(DrawCanvasWidget ww, const char *name);
static void free_pixmaps(DrawCanvasWidget ww);
static void deinit_pixmaps(DrawCanvasWidget ww);

#if 0
static int find_string(const char *s, char **array, int num_strings);
#endif
static int find_case_string(const char *s, char **array, int num_strings);

DrawCanvasClassRec drawCanvasClassRec = {
  { /* CoreClassPart */
    (WidgetClass)&widgetClassRec, /* superclass */
    "DrawCanvas",		/* class_name */
    sizeof(DrawCanvasRec),	/* widget_size */
    NULL,			/* class_initialize */
    NULL,			/* class_part_initialize */
    False,			/* class_inited */
    Initialize,			/* initialize */
    NULL,			/* initialize_hook */
    XtInheritRealize,		/* realize */
    NULL,			/* actions */
    0,				/* num_actions */
    resources,			/* resources */
    XtNumber(resources),	/* num_resources */
    NULLQUARK,			/* xrm_class */
    True,			/* compress_motion */
    XtExposeNoCompress,         /* compress_exposure */
    True,			/* compress_enterleave */
    True,			/* visible_interest */
    Destroy,			/* destroy */
    Resize,			/* resize */
    Redisplay,			/* expose */
    SetValues,			/* set_values */
    NULL,			/* set_values_hook */
    XtInheritSetValuesAlmost,	/* set_values_almost */
    NULL,			/* get_values_hook */
    NULL,			/* accept_focus */
    XtVersion,			/* version */
    NULL,			/* callback_private */
    XtInheritTranslations,	/* tm_table */
    QueryGeometry,		/* query_geometry */
    XtInheritDisplayAccelerator, /* display_accelerator */
    NULL			/* extension */
  }, { /* DrawCanvasClassPart */
    0				/* empty */
  }
};

WidgetClass drawCanvasWidgetClass = (WidgetClass) &drawCanvasClassRec;

/*
 * Widget interface to Xt (methods)
 */

/* The initialize method */
static void Initialize(Widget request, Widget new,
  ArgList args, Cardinal *num_args)
{
  DrawCanvasWidget neww = (DrawCanvasWidget) new;

  /* We don't have resources to check */

  /* Set the default width and height, if wanted */
  if (neww->core.width == 0)
    neww->core.width = DEF_WIDTH;
  if (neww->core.height == 0)
    neww->core.height = DEF_HEIGHT;

  /* Initialise things */
  neww->draw.should_exit = 0;
  neww->draw.retval_exists = 0;
  neww->draw.retval_str[0] = 0;
  neww->draw.drawinwindow_set = 1;
  neww->draw.processexpose = 1;
  neww->draw.reportexpose = 0;
  neww->draw.reportresize = 0;
  neww->draw.eventmask = 0;
  neww->draw.exit_on_event = 0;
  neww->draw.gc_stack = NULL; init_gcs(neww);
  neww->draw.winpmwidth = -1; init_winpm(neww);
  neww->draw.pm = neww->draw.winpm;
  neww->draw.drawinwindow = 1;
  neww->draw.colors = NULL; init_colors(neww);
  neww->draw.fonts = NULL; init_fonts(neww);
  neww->draw.pixmaps = NULL; init_pixmaps(neww);
}

/* The expose method */
static void Redisplay(Widget w, XEvent *event, Region region)
{
  DrawCanvasWidget ww = (DrawCanvasWidget) w;
  XExposeEvent *expe = (XExposeEvent *) event;

  if (ww->draw.reportexpose)
    eventf(ww, "Expose %d %d %d %d %d",
      expe->x, expe->y, expe->width, expe->height, expe->count);

  if (!ww->draw.processexpose)
    return; /* Don't want to process expose events right now */

  if (!XtIsRealized(w) || !ww->core.visible)
    return; /* Don't draw to a window that isn't visible */

  /* We simply copy the appropriate area from our buffer pixmap */
  XCopyArea(XtDisplay(w), ww->draw.winpm, XtWindow(w), ww->draw.pmgc,
    expe->x, expe->y, expe->width, expe->height, expe->x, expe->y);
}

/* The resize method */
static void Resize(Widget w)
{
  DrawCanvasWidget ww = (DrawCanvasWidget) w;

  if (ww->draw.reportresize)
    eventf(ww, "Resize %d %d", ww->core.width, ww->core.height);

  /* Resize our pixmap accordingly */
  resize_winpm(ww, ww->core.width, ww->core.height);
}

/* The set_values method */
static Boolean SetValues(Widget current, Widget request, Widget new,
			 ArgList args, Cardinal *num_args)
{
  /*
   * We don't have any resources to check...
   * (foreground is just a default, so we don't care if it changes)
   */

  return False;
}

/* The query_geometry method */
static XtGeometryResult QueryGeometry(Widget w,
  XtWidgetGeometry *request, XtWidgetGeometry *reply)
{
  DrawCanvasWidget ww = (DrawCanvasWidget) w;

  /* We prefer the size of our pixmap */
  reply->request_mode = CWWidth | CWHeight;
  reply->width = ww->draw.winpmwidth;
  reply->height = ww->draw.winpmheight;

  if ((!(request->request_mode & CWWidth) ||
       request->width == reply->width) &&
      (!(request->request_mode & CWHeight) ||
       request->height == reply->height)) return XtGeometryYes;
  else if (reply->width == ww->core.width &&
    reply->height == ww->core.height) return XtGeometryNo;
  else return XtGeometryAlmost;
}

/* The destroy method */
static void Destroy(Widget w)
{
  DrawCanvasWidget ww = (DrawCanvasWidget) w;

  /* Deinitialise things and free all of our allocated data */
  deinit_winpm(ww);
  deinit_colors(ww);
  deinit_fonts(ww);
  deinit_gcs(ww);
  deinit_pixmaps(ww);
}

/*
 * DrawCanvas-commands.c contains functions for all of the commands we
 * accept, lists of their names (cmd_names) and function pointers
 * (cmd_funcs), and the number of commands (num_cmds).
 *
 * This isn't pretty, but it's better than having everything in here,
 * since an Xt widget is supposed to be in a single object file... It
 * also keeps the global namespace somewhat cleaner.
 */
#include "DrawCanvas-commands.c"

/*
 * Public functions
 */

char *DrawCanvasCommand(Widget w, const char *command)
{
  DrawCanvasWidget ww = (DrawCanvasWidget) w;
  char *cmdp;
  int i;

  /* Start up the parser */
  start_parse(ww, command);

  /* Skip any initial whitespace */
  parse_whitespace(ww);

  /* Parse the command name (which is a symbol) */
  if (!parse_symbol(ww, &cmdp))
    return NULL;

  /* Find the appropriate function */
  i = find_case_string(cmdp, cmd_names, num_cmds);

  if (i >= 0)
  {
    /* Call the command handler */
    ww->draw.retval_exists = 0;
    if (((cmd_funcs[i])(ww)) == 0)
      return NULL; /* The handler failed (usually due to a parse error) */
  }
  else
  {
    parse_error(ww, 0, "Unknown command.");
    return NULL;
  }

  /* We were successful; let the parser clean up */
  end_parse(ww);

  if (ww->draw.retval_exists)
    return ww->draw.retval_str;
  else
    return NULL;
}

int DrawCanvasShouldExit(Widget w)
{
  DrawCanvasWidget ww = (DrawCanvasWidget) w;

  return ww->draw.should_exit;
}

/*
 * Parsing
 */

/* Initialise the parser */
static void start_parse(DrawCanvasWidget ww, const char *command)
{
  ww->draw.parse_origcmd = (char *) command;
  ww->draw.parse_cmd = XtNewString(command);
  ww->draw.parse_pos = ww->draw.parse_cmd;
  ww->draw.parse_allocp = NULL;
}

/*
 * XtMalloc something and save the pointer so that end_parse() frees
 * it automatically. Returns a pointer to the allocated data.
 *
 * (XXX) We should support more than one allocation per command, but
 * we don't need that yet...
 */
static void *parse_malloc(DrawCanvasWidget ww, int size)
{
  /*
   * Warn about calling me twice, but don't do anything about it.
   * If this happens, the first allocation is never freed.
   */
  if (ww->draw.parse_allocp != NULL)
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: parse_malloc called twice, leaking memory!");

  ww->draw.parse_allocp = (void *) XtMalloc(size);

  return ww->draw.parse_allocp;
}

/*
 * Count whitespace characters starting from pos
 * Returns the number of whitespace characters skipped, or 0 if none
 */
static int parse_count_whitespace(DrawCanvasWidget ww, const char *pos)
{
  int count = 0;

  if (*pos == 0)
    return 0; /* Empty string, so no whitespace found */

  /* Spaces, tabs, and newlines are whitespace */
  while (*pos != 0 && strchr(" \t\n", *pos) != NULL)
  {
    pos++;
    count++;
  }
  
  return count;
}

/*
 * Parse (skip over) whitespace, if any
 */
static void parse_whitespace(DrawCanvasWidget ww)
{
  if (ww->draw.parse_pos == NULL)
    /* No command, so no whitespace (but it's not an error either) */
    return;

  ww->draw.parse_pos +=
    parse_count_whitespace(ww, ww->draw.parse_pos);

  if (*ww->draw.parse_pos == 0)
    ww->draw.parse_pos = NULL; /* The command ends here */
}

/*
 * Parse the end of a token, setting parse_pos to the start of the
 * next token (skipping over the whitespace between tokens).
 *
 * endpos is the pointer to the first character after the token (which
 * should be whitespace or NUL). If zero is true, *endpos is set to NUL.
 *
 * Returns 1 if ok, 0 if not
 */
static int parse_token_end(DrawCanvasWidget ww, char *endpos, int zero)
{  
  int ws_count;

  if (*endpos == 0)
  {
    /* No characters left in the command */
    ww->draw.parse_pos = NULL;
    return 1;
  }

  ws_count = parse_count_whitespace(ww, endpos);
  
  if (ws_count == 0)
  {
    /* Show the (first) invalid character */
    parse_error(ww, 1, "Invalid character after token.");
    return 0;
  }

  if (*(endpos + ws_count) != 0)
    ww->draw.parse_pos = endpos + ws_count; /* The next token is here */
  else ww->draw.parse_pos = NULL; /* The command ends now */

  if (zero)
    *endpos = 0;

  return 1;
}

/*
 * Parses the next token as a symbol ([a-zA-Z][a-zA-Z0-9]*).
 * Sets *value to a pointer to the (null-terminated) symbol name.
 * Returns 1 if ok, 0 if not
 */
static int parse_symbol(DrawCanvasWidget ww, char **value)
{
  char *sp, *endp;

  sp = ww->draw.parse_pos;

  if (sp == NULL || *sp == 0)
  {
    parse_error(ww, 0,
      "End of command reached while expecting a symbol.");
    return 0;
  }

  if (!isalpha(*sp))
  {
    /* Show the start of the "something else" */
    parse_error(ww, 1, "Expected a symbol, got something else.");
    return 0;
  }

  /* Find the end of the symbol */
  for (endp = sp + 1; *endp != 0; endp++)
    if (!isalnum(*endp))
      break;

  if (parse_token_end(ww, endp, 1) == 0)
    return 0; /* A parse error immediately after our token */

  *value = sp;

  return 1;
}

/*
 * Parses the next token as a decimal integer ([-+]*[0-9]*).
 * Sets *value to the integer parsed.
 * Returns 1 if ok, 0 if not
 */
static int parse_int(DrawCanvasWidget ww, int *value)
{
  char *sp, *endp;
  /* Multiplier for the integer; set to -1 for negative values */
  int negmult = 1;

  sp = ww->draw.parse_pos;

  if (sp == NULL || *sp == 0)
  {
    parse_error(ww, 0,
      "End of command reached while expecting an integer.");
    return 0;
  }

  while (*sp == '+' || *sp == '-')
  {
    if (*sp == '-')
      negmult = -negmult;
    sp++;
  }

  if (!isdigit(*sp))
  {
    /* Show the start of the "something else" */
    parse_error(ww, 1, "Expected an integer, got something else.");
    return 0;
  }

  /* Find the end of the integer */
  for (endp = sp + 1; *endp != 0; endp++)
    if (!isdigit(*endp))
      break;

  if (parse_token_end(ww, endp, 1) == 0)
    return 0; /* A parse error immediately after our token */

  *value = negmult * atoi(sp);

  return 1;
}

/*
 * Parses the next token as a string (".*").
 * Sets *value to a pointer to the (unescaped) string.
 * Returns 1 if ok, 0 if not
 */
static int parse_string(DrawCanvasWidget ww, char **value)
{
  char *sp, *srcp, *destp;
  int esc_mode;

  sp = ww->draw.parse_pos;

  if (sp == NULL || *sp == 0)
  {
    parse_error(ww, 0,
      "End of command reached while expecting a string.");
    return 0;
  }

  if (*sp != '"')
  {
    /* Show the start of the "something else" */
    parse_error(ww, 1, "Expected a string, got something else.");
    return 0;
  }

  sp++; /* Skip past the initial '"' */

  esc_mode = 0;

  for (destp = srcp = sp; ; srcp++)
  {
    if (*srcp == 0)
    {
      /* Show the start of the string */
      parse_error(ww, 1, "Unterminated string.");
      return 0;
    }

    if (esc_mode)
    {
      switch (*srcp) {
        case 'n': *destp = '\n'; break;
        /* \\ and \" work with the default case */
        default: *destp = *srcp; break;
      }

      esc_mode = 0;
      destp++;
      continue;
    } else if (*srcp == '"') {
      /* End of string reached */
      break;
    }
     
    if (*srcp == '\\')
    {
      esc_mode = 1;
      continue;
    }

    /* It's an ordinary character */
    *destp = *srcp;
    destp++;
  }

  *destp = 0;

  /* Skip past the terminating '"', and end the string there */
  *srcp = 0;
  srcp++;

  if (parse_token_end(ww, srcp, 0) == 0)
    return 0; /* A parse error immediately after our token */

  *value = sp;

  return 1;
}

/*
 * Parses the next token as a special character (ch)
 * Returns 1 if ok, 0 if not
 */
static int parse_special_char(DrawCanvasWidget ww, char ch)
{
  static char errend[] = "End of command reached while expecting '?'.";
  static char errnf[] = "'?' expected.";
  char *sp;

  sp = ww->draw.parse_pos;

  if (sp == NULL || *sp == 0)
  {
    /* Insert ch into the error message */
    errend[40] = ch;

    parse_error(ww, 0, errend);
    return 0;
  }

  if (*sp != ch)
  {
    /* Insert ch into the error message */
    errnf[1] = ch;

    /* Show where the character was supposed to be */
    parse_error(ww, 1, errnf);
    return 0;
  }

  sp++; /* Skip over the character */

  if (parse_token_end(ww, sp, 0) == 0)
    return 0; /* A parse error immediately after our token */

  return 1;
}

/* Parse '(' '(' */
static int parse_list_start(DrawCanvasWidget ww)
{
  if (!parse_special_char(ww, '('))
    return 0;

  return parse_special_char(ww, '(');
}

/* Parse ')' '(' */
static int parse_list_next_element(DrawCanvasWidget ww)
{
  if (!parse_special_char(ww, ')'))
    return 0;

  return parse_special_char(ww, '(');
}

/* Parse ')' ')' */
static int parse_list_end(DrawCanvasWidget ww)
{
  if (!parse_special_char(ww, ')'))
    return 0;

  return parse_special_char(ww, ')');
}

/*
 * Looks ahead and counts the number of elements in a list
 * Should be called after parse_list_start()
 * Sets *value to the count
 * Returns 1 if ok, 0 if not
 */
static int parse_list_count_elements(DrawCanvasWidget ww, int *value)
{
  char *sp;
  int count, nest;

  /*
   * (XXX) Strings containing '(' or ')' inside lists don't work, but
   * none of our commands accepts strings inside of lists anyway...
   */
  for (count = 0, nest = 1, sp = ww->draw.parse_pos; ; sp++)
  {
    if (*sp == 0)
    {
      /*
       * Show the first element of the list (because that is what
       * parse_pos is set at...)
       */
      parse_error(ww, 1, "Unterminated list (unbalanced parentheses).");

      return 0;
    }

    if (*sp == '(')
    {
      /* New sublist */
      nest++;
    } else if (*sp == ')')
    {
      if (nest > 0)
      {
	/* End of a sublist */
	nest--;
	if (nest == 0)
	  count++;
      } else break; /* End of our list */
    }
  }

  *value = count;

  return 1;
}

/*
 * Parse the end of a command (signaling an error if there is data
 * after the command.
 * Returns 1 if ok, 0 if not
 */
static int parse_command_end(DrawCanvasWidget ww)
{
  /*
   * This isn't actually necessary, since parse_token_end() does it,
   * but this way we support an empty command too.
   */
  parse_whitespace(ww);

  if (ww->draw.parse_pos != NULL && *ww->draw.parse_pos != 0)
  {
    parse_error(ww, 1, "Additional data found after a valid command.");
    return 0;
  }

  ww->draw.parse_pos = NULL;

  return 1;
}

/*
 * Parsing failed, report the error and show parse_origcmd.
 * If showpos is true, show parse_pos.
 */

#define PARSE_ERROR_MSGLEN 150 /* How much of msg to show */
#define PARSE_ERROR_CMDLEN 70 /* How much of the command to show */

static void parse_error(DrawCanvasWidget ww, int showpos, char *msg)
{
  /* "DrawCanvas..." + msg + (cmd+mark+2*"  ...\n") + NUL */
  char wbuf[26 + PARSE_ERROR_MSGLEN + 2 * (PARSE_ERROR_CMDLEN + 6) + 1];
  int wbuf_len; /* Invariant = strlen(wbuf) */
  int origcmd_len = strlen(ww->draw.parse_origcmd);
  int markpos;

  wbuf_len = sprintf(wbuf, "DrawCanvas: Parse error: %.*s\n",
    PARSE_ERROR_MSGLEN, msg);

  /* Now there are at least PARSE_ERROR_CMDLEN characters left in wbuf */
  
  /* Print the command */
  wbuf_len += sprintf(&wbuf[wbuf_len], "  %.*s%s\n",
    PARSE_ERROR_CMDLEN, ww->draw.parse_origcmd,
    (origcmd_len > PARSE_ERROR_CMDLEN) ? "..." : "");

  /* Print the mark, if wanted */
  if (showpos)
  {
    markpos = (ww->draw.parse_pos != NULL)
      ? ww->draw.parse_pos - ww->draw.parse_cmd : origcmd_len;

    if (markpos > PARSE_ERROR_CMDLEN)
      markpos = PARSE_ERROR_CMDLEN + 1; /* Mark the middle '.' */
    
    /*
     * Print "  " + markpos spaces + "^\n"
     * (" %..." and markpos + 1 so that the width is always > 0)
     */
    wbuf_len += sprintf(&wbuf[wbuf_len], " %*s^\n",
      markpos + 1, " ");
  }

  XtAppWarning(XtWidgetToApplicationContext((Widget) ww), wbuf);

  end_parse(ww);
}

/* Clean up after parsing, freeing possible allocated data */
static void end_parse(DrawCanvasWidget ww)
{
  if (ww->draw.parse_allocp != NULL)
  {
    /* Someone has called parse_malloc(); free the stuff */
    XtFree((char *) ww->draw.parse_allocp);
    ww->draw.parse_allocp = NULL;
  }

  XtFree(ww->draw.parse_cmd);
}

/*
 * Return values and events
 */

/*
 * Return the string constructed from the arguments (printf-like).
 * Should be called from a command (i.e. inside DrawCanvasCommand).
 */
static void returnf(DrawCanvasWidget ww, char *fmt, ...)
{
  va_list args;

  va_start(args, fmt);

  /* (XXX) Use vsnprintf where available */
  vsprintf(ww->draw.retval_str, fmt, args);

  ww->draw.retval_exists = 1;

  va_end(args);
}

/*
 * Send the event string constructed from the arguments (printf-like)
 * and set should_exit if exit_on_event is true.
 */
static void eventf(DrawCanvasWidget ww, char *fmt, ...)
{
  char event_str[EVENT_STR_SIZE];
  va_list args;

  if (ww->draw.exit_on_event)
    ww->draw.should_exit = 1;

  va_start(args, fmt);

  /* (XXX) Use vsnprintf where available */
  vsprintf(event_str, fmt, args);

  XtCallCallbackList((Widget) ww, ww->draw.callback,
    (XtPointer) event_str);

  va_end(args);
}

/*
 * Change event_handler's current event mask by adding the bits in add
 * and removing the bits in remove.
 */
static void change_event_mask(DrawCanvasWidget ww,
  EventMask add, EventMask remove)
{
  Widget w = (Widget) ww;
  EventMask oldmask = ww->draw.eventmask;
  EventMask newmask = (oldmask | add) & (~remove);

  if (oldmask)
    XtRemoveEventHandler(w, oldmask, False, event_handler, NULL);

  if (newmask)
    XtAddEventHandler(w, newmask, False, event_handler, NULL);

  ww->draw.eventmask = newmask;
}

/* The event handler reports every known event it gets. */
static void event_handler(Widget w, XtPointer closure, XEvent *event,
  Boolean *continue_to_dispatch)
{
  DrawCanvasWidget ww = (DrawCanvasWidget) w;
  KeySym ks;
  char *ks_string;

  switch (event->type)
  {
    case KeyPress:
    case KeyRelease:
      XLookupString((XKeyEvent *) event, NULL, 0, &ks, NULL);
      ks_string = XKeysymToString(ks);
      eventf(ww, "Key%s %d %d %s \"%s\"",
        (event->type == KeyPress) ? "Press" : "Release",
	event->xkey.x,
	event->xkey.y,
	statestr(event->xkey.state),
	(ks_string == NULL) ? "NULL" : ks_string);
      break;
    case ButtonPress:
    case ButtonRelease:
      eventf(ww, "Button%s %d %d %s %d",
        (event->type == ButtonPress) ? "Press" : "Release",
	event->xbutton.x,
	event->xbutton.y,
	statestr(event->xbutton.state),
	(event->xbutton.button == Button1) ? 1 :
	(event->xbutton.button == Button2) ? 2 :
	(event->xbutton.button == Button3) ? 3 :
	(event->xbutton.button == Button4) ? 4 :
	(event->xbutton.button == Button5) ? 5 :
	event->xbutton.button);
      break;
    case MotionNotify:
      eventf(ww, "MotionNotify %d %d %s %s",
        event->xmotion.x, event->xmotion.y, statestr(event->xmotion.state),
        event->xmotion.is_hint ? "Hint" : "Normal");
      break;
    case EnterNotify:
    case LeaveNotify:
      eventf(ww, "%sNotify %s %s",
	(event->type == EnterNotify) ? "Enter" : "Leave",
	(event->xcrossing.focus == True) ? "True" : "False",
	statestr(event->xcrossing.state));
      break;
    case FocusIn:
      eventf(ww, "FocusIn");
      break;
    case FocusOut:
      eventf(ww, "FocusOut");
      break;
    default:
      break; /* Ignore unknown events */
  }
}

/*
 * Convert a key/button state to a string. Returns the string, which
 * is overwritten on the next call.
 */
static char *statestr(unsigned int state)
{
  static unsigned int state_flags[] = {
    Button1Mask, Button2Mask, Button3Mask, Button4Mask, Button5Mask,
    ControlMask, LockMask,
    Mod1Mask, Mod2Mask, Mod3Mask, Mod4Mask, Mod5Mask,
    ShiftMask
  };
  static char *state_strings[] = {
    " Button1", " Button2", " Button3", " Button4", " Button5",
    " Control", " Lock", " Mod1", " Mod2", " Mod3", " Mod4", " Mod5",
    " Shift"
  };
  static int num_states = 13;
  static char buf[256];

  int i;

  strcpy(buf, "(");

  for (i = 0; i < num_states; i++)
    if (state & state_flags[i])
      strcat(buf, state_strings[i]);

  strcat(buf, " )");

  return buf;
}

/*
 * Colors
 */

/* Initialise the colors cache */
static void init_colors(DrawCanvasWidget ww)
{
  if (ww->draw.colors != NULL)
    return;

  ww->draw.colors = dict_create();
}

/*
 * Get a named color from the color cache, allocating the color
 * if it isn't there
 */
static unsigned long get_color(DrawCanvasWidget ww, const char *name)
{
  DictData *ddp = dict_find(ww->draw.colors, name);
  DictData dd;
  XColor xcolor;

  if (ddp == NULL) /* We didn't find it, so allocate it */
  {
    if (XAllocNamedColor(XtDisplay(ww), ww->core.colormap, name,
      &xcolor, &xcolor) == 0)
    {
      XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
        "DrawCanvas: Could not allocate a color.");
      
      /* (XXX) Search already-allocated colors for a close match? */
      /* (XXX) Don't put this in the cache? */
      dd.ul = BlackPixelOfScreen(XtScreen(ww));
    } else dd.ul = xcolor.pixel;

    dict_insert(ww->draw.colors, name, dd);
    ddp = &dd;
  }

  return ddp->ul;
}

/* Free all allocated colors (but not the table of colors) */
static void free_colors(DrawCanvasWidget ww)
{
  unsigned long c[1];

  /*
   * (XXX) We could free all colors in one XFreeColors, but this would
   * break the encapsulation of the dictionary
   */
  while (!dict_emptyp(ww->draw.colors)) {
    c[0] = dict_remove_any(ww->draw.colors).ul;
    XFreeColors(XtDisplay(ww), ww->core.colormap, c, 1, 0);
  }
}

/* Free everything related to the color cache */
static void deinit_colors(DrawCanvasWidget ww)
{
  if (ww->draw.colors == NULL)
    return;

  free_colors(ww);

  dict_destroy(ww->draw.colors);
  ww->draw.colors = NULL;
}

/*
 * Fonts
 */

/* Initialise the fonts cache */
static void init_fonts(DrawCanvasWidget ww)
{
  if (ww->draw.fonts != NULL)
    return;

  ww->draw.fonts = dict_create();
}

/*
 * Get a named font from the fonts cache, loading the font
 * if it isn't there
 */
static Font get_font(DrawCanvasWidget ww, const char *name)
{
  DictData *ddp = dict_find(ww->draw.fonts, name);
  DictData dd;

  if (ddp == NULL) /* We didn't find it, so allocate it */
  {
    /* (XXX) Error handling? */
    dd.f = XLoadFont(XtDisplay(ww), name);

    dict_insert(ww->draw.fonts, name, dd);
    ddp = &dd;
  }

  return ddp->f;
}

/* Free all allocated fonts (but not the table of fonts) */
static void free_fonts(DrawCanvasWidget ww)
{
  while (!dict_emptyp(ww->draw.fonts))
    XUnloadFont(XtDisplay(ww),
      dict_remove_any(ww->draw.fonts).f);
}

/* Free everything related to the font cache */
static void deinit_fonts(DrawCanvasWidget ww)
{
  if (ww->draw.fonts == NULL)
    return;

  free_fonts(ww);

  dict_destroy(ww->draw.fonts);
  ww->draw.fonts = NULL;
}

/*
 * GCs
 */

/*
 * Initially allocate space for GCSTACK_INIT_ALLOC GCs in the GC stack.
 * Each time it's full, allocate GCSTACK_ALLOC_MORE more.
 */
#define GCSTACK_INIT_ALLOC 5
#define GCSTACK_ALLOC_MORE 5

/* Allocate the initial GCs */
static void init_gcs(DrawCanvasWidget ww)
{
  XGCValues gcv;

  if (ww->draw.gc_stack != NULL)
    return; /* Already initialised */

  /* pmgc is used for clearing and copying the pixmap */
  gcv.background = ww->core.background_pixel;
  gcv.foreground = ww->core.background_pixel;

  /* XCreateGC instead of XtGetGC because CopyFromPixmap modifies the
   * GC temporarily. */
  ww->draw.pmgc = XCreateGC(XtDisplay(ww),
    RootWindowOfScreen(XtScreen(ww)),
    GCForeground | GCBackground, &gcv);

  /* dgc is for drawing */
  new_dgc(ww);

  /* Allocate and initialise the GC stack */

  ww->draw.gc_stack_size = GCSTACK_INIT_ALLOC;

  ww->draw.gc_stack = (GC *)
    XtMalloc(ww->draw.gc_stack_size * sizeof(GC));

  ww->draw.gc_stack_pointer = 0; /* Empty stack */
}

/* Free dgc */
static void free_dgc(DrawCanvasWidget ww)
{
  XFreeGC(XtDisplay(ww), ww->draw.dgc);
}

/* Allocate a new dgc */
static void new_dgc(DrawCanvasWidget ww)
{
  XGCValues gcv;

  gcv.background = ww->core.background_pixel;
  gcv.foreground = ww->draw.foreground;
  
  ww->draw.dgc = XCreateGC(XtDisplay(ww),
    RootWindowOfScreen(XtScreen(ww)),
    GCForeground | GCBackground, &gcv);
}

/* Push dgc on the stack and allocate a new dgc */
static void gcstack_push_and_reset(DrawCanvasWidget ww)
{
  /* Push the current dgc on the stack */

  if (ww->draw.gc_stack_pointer == ww->draw.gc_stack_size)
  {
    /* GC stack full, allocate more space */

    ww->draw.gc_stack_size += GCSTACK_ALLOC_MORE;

    ww->draw.gc_stack = (GC *) XtRealloc((char *) ww->draw.gc_stack,
      ww->draw.gc_stack_size);
  }

  ww->draw.gc_stack[ww->draw.gc_stack_pointer] = ww->draw.dgc;
  ww->draw.gc_stack_pointer++;

  /* Allocate the new dgc */
  new_dgc(ww);
}

/* Free dgc and pop a new dgc from the stack */
static void gcstack_pop(DrawCanvasWidget ww)
{
  if (ww->draw.gc_stack_pointer <= 0)
  {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: Tried to pop from an empty GC stack.");
    return;
  }

  free_dgc(ww);

  ww->draw.gc_stack_pointer--;
  ww->draw.dgc = ww->draw.gc_stack[ww->draw.gc_stack_pointer];
}

/* Free the initial GCs and the GC stack */
static void deinit_gcs(DrawCanvasWidget ww)
{
  int i;
  Display *disp = XtDisplay(ww);

  if (ww->draw.gc_stack == NULL)
    return; /* Already deinited */

  /* The GC stack */

  for (i = 0; i < ww->draw.gc_stack_pointer; i++)
    XFreeGC(disp, ww->draw.gc_stack[i]);

  XtFree((char *) ww->draw.gc_stack);
  ww->draw.gc_stack = NULL;

  /* The initial GCs */
  free_dgc(ww);
  XtReleaseGC((Widget) ww, ww->draw.pmgc);
}

/*
 * The pixmap that works as a buffer for our window (winpm)
 */

/*
 * Create the buffer pixmap winpm
 * init_gcs() needs to be done before this
 */
static void init_winpm(DrawCanvasWidget ww)
{
  if (ww->draw.winpmwidth != -1)
    return;

  ww->draw.winpmwidth = ww->core.width;
  ww->draw.winpmheight = ww->core.height;

  ww->draw.winpm = XCreatePixmap(XtDisplay(ww),
    RootWindowOfScreen(XtScreen(ww)),
    ww->draw.winpmwidth, ww->draw.winpmheight,
    DefaultDepthOfScreen(XtScreen(ww)));

  clear_winpm(ww);
}

/* Clear winpm */
static void clear_winpm(DrawCanvasWidget ww)
{
  XFillRectangle(XtDisplay(ww), ww->draw.winpm, ww->draw.pmgc,
    0, 0, ww->draw.winpmwidth + 1, ww->draw.winpmheight + 1);
}

/* Resize winpm to the given size */
static void resize_winpm(DrawCanvasWidget ww, int width, int height)
{
  Pixmap oldpm = ww->draw.winpm;
  int oldwidth = ww->draw.winpmwidth, oldheight = ww->draw.winpmheight;

  if (width == oldwidth && height == oldheight)
    return; /* Size not changed, do nothing */

  /* Create and clear the new pixmap */
  ww->draw.winpm = XCreatePixmap(XtDisplay(ww),
    RootWindowOfScreen(XtScreen(ww)), width, height,
    DefaultDepthOfScreen(XtScreen(ww)));
  ww->draw.winpmwidth = width;
  ww->draw.winpmheight = height;
  clear_winpm(ww);

  /* Copy the old pixmap to the new one */
  XCopyArea(XtDisplay(ww), oldpm, ww->draw.winpm, ww->draw.pmgc,
    0, 0, oldwidth, oldheight, 0, 0);

  /* Update pm if necessary */
  if (ww->draw.pm == oldpm)
    ww->draw.pm = ww->draw.winpm;

  /* Free the old pixmap */
  XFreePixmap(XtDisplay(ww), oldpm);
}

/* Free the buffer pixmap */
static void deinit_winpm(DrawCanvasWidget ww)
{
  if (ww->draw.winpmwidth == -1)
    return;

  XFreePixmap(XtDisplay(ww), ww->draw.winpm);

  ww->draw.winpmwidth = -1;
  ww->draw.winpmheight = -1;
}

/*
 * User-created pixmaps
 */

/* Initialise storage for pixmaps */
static void init_pixmaps(DrawCanvasWidget ww)
{
  if (ww->draw.pixmaps != NULL)
    return;

  ww->draw.pixmaps = dict_create();
}

/*
 * Create a new, empty pixmap and enter it into the storage
 * Returns 1 if ok, 0 if failed.
 */
static int new_pixmap(DrawCanvasWidget ww, const char *name,
  int width, int height)
{
  DictData dd;
  GC bmgc;
  XGCValues gcv;

  if (find_pixmap(ww, name, NULL, NULL))
    return 0; /* Already exists */

  dd.pm.p = XCreatePixmap(XtDisplay(ww), RootWindowOfScreen(XtScreen(ww)),
    width, height, DefaultDepthOfScreen(XtScreen(ww)));
  dd.pm.s = XCreatePixmap(XtDisplay(ww), RootWindowOfScreen(XtScreen(ww)),
    width, height, 1);

  /* Clear the new pixmap */
  XFillRectangle(XtDisplay(ww), dd.pm.p, ww->draw.pmgc,
    0, 0, width + 1, height + 1);

  /* Fill the shapemask with 1s. We need to create a GC for the
   * operation... XXX Share this GC with something that the user can
   * use for drawing into the shapemask. */
  gcv.background = 0;
  gcv.foreground = 1;
  bmgc = XCreateGC(XtDisplay(ww), dd.pm.s,
    GCForeground | GCBackground, &gcv);
  XFillRectangle(XtDisplay(ww), dd.pm.s, bmgc,
    0, 0, width + 1, height + 1);
  XFreeGC(XtDisplay(ww), bmgc);

  dict_insert(ww->draw.pixmaps, name, dd);

  return 1; /* Ok */
}

/* Insert a pixmap into the dictionary; returns 1 if ok */
static int add_pixmap(DrawCanvasWidget ww, const char *name,
  Pixmap pixmap, Pixmap shapemask)
{
  DictData dd;

  if (find_pixmap(ww, name, NULL, NULL))
    return 0; /* Already exists */

  dd.pm.p = pixmap;
  dd.pm.s = shapemask;
  dict_insert(ww->draw.pixmaps, name, dd);

  return 1; /* Ok */
}

/*
 * Find a pixmap from the storage. Sets *pixmap_return to the pixmap
 * found, and *shapemask_return to the corresponding shapemask.
 * Returns 1 if found, 0 otherwise.
 */
static int find_pixmap(DrawCanvasWidget ww, const char *name,
  Pixmap *pixmap_return, Pixmap *shapemask_return)
{
  DictData *ddp = dict_find(ww->draw.pixmaps, name);

  if (ddp == NULL)
    return 0; /* Not found */

  if (pixmap_return != NULL)
    *pixmap_return = ddp->pm.p;

  if (shapemask_return != NULL)
    *shapemask_return = ddp->pm.s;

  return 1; /* Found */
}

/* Remove and free a pixmap from the storage; returns 1 if ok */
static int free_pixmap(DrawCanvasWidget ww, const char *name)
{
  DictData *ddp = dict_find(ww->draw.pixmaps, name);

  if (ddp == NULL)
    return 0; /* Not found */

  XFreePixmap(XtDisplay(ww), ddp->pm.p);
  XFreePixmap(XtDisplay(ww), ddp->pm.s);
  dict_remove(ww->draw.pixmaps, name);

  return 1; /* Ok */
}

/* Free all pixmaps in the storage */
static void free_pixmaps(DrawCanvasWidget ww)
{
  DictData dd;

  while (!dict_emptyp(ww->draw.pixmaps)) {
    dd = dict_remove_any(ww->draw.pixmaps);
    XFreePixmap(XtDisplay(ww), dd.pm.p);
    XFreePixmap(XtDisplay(ww), dd.pm.s);
  }
}

/* Deinitialise pixmap storage */
static void deinit_pixmaps(DrawCanvasWidget ww)
{
  if (ww->draw.pixmaps == NULL)
    return;

  free_pixmaps(ww);

  dict_destroy(ww->draw.pixmaps);
  ww->draw.pixmaps = NULL;
}

/*
 * Utility functions
 */

#if 0 /* Not needed anymore */
/*
 * Find the string s in an array of strings, case-sensitively
 * Returns the array index of the found string, or -1 if not found
 */
static int find_string(const char *s, char **array, int num_strings)
{
  int i;

  for (i = 0; i < num_strings; i++)
    if (strcmp(s, array[i]) == 0)
      return i; /* Found! */

  return -1; /* Not found */
}
#endif

/*
 * Find the string s in an array of strings, ignoring case
 * Returns the array index of the found string, or -1 if not found
 */
static int find_case_string(const char *s, char **array, int num_strings)
{
  int i;

  for (i = 0; i < num_strings; i++)
    if (strcasecmp(s, array[i]) == 0)
      return i; /* Found! */

  return -1; /* Not found */
}
