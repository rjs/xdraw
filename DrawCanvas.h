/*
 * DrawCanvas -- A widget that accepts drawing commands
 * Copyright (C) 1998-2001 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

/*
 * DrawCanvas.h -- Public header file
 */

#ifndef _DrawCanvas_h
#define _DrawCanvas_h

/* ***** DrawCanvas widget ***** */

/*
 * Additional resources (superclass=Core):
 *
 * Name           Class           Type           Default value        Note
 * ----           -----           ----           -------------        ----
 * allowFileRead  AllowFileAccess Boolean        True                 [1]
 * allowFileWrite AllowFileAccess Boolean        True                 [2]
 * callback       Callback        XtCallbackList NULL                 [3]
 * foreground     Foreground      Pixel          XtDefaultForeground  [4]
 *
 * Notes:
 * [1] Whether to allow reading files from the file system, for reading
 *     XPM files.
 * [2] Whether to allow writing files to the file system, for dumping
 *     pixmaps to XPM files.
 * [3] Callback for events. Called when events occur that are turned
 *     on by the appropriate commands. The call_data parameter of the
 *     callback will point to a string with the description of the
 *     event.
 * [4] Initial foreground color.
 */

#define XtNallowFileRead "allowFileRead"
#define XtNallowFileWrite "allowFileWrite"
#define XtCAllowFileAccess "AllowFileAccess"

typedef struct _DrawCanvasClassRec *DrawCanvasWidgetClass;
typedef struct _DrawCanvasRec *DrawCanvasWidget;

extern WidgetClass drawCanvasWidgetClass;

/*
 * Use this to send drawing commands to the DrawCanvas widget.
 *
 * command is a null-terminated string containing the command (without
 * the trailing newline). DrawCanvasCommand() displays parse errors
 * etc. using XtWarning(3x), and ignores the erroneous commands.
 *
 * Returns either NULL or a pointer to a return value (a string).
 * The string is overwritten on the next call to DrawCanvasCommand.
 */
extern char *DrawCanvasCommand(Widget w, const char *command);

/*
 * Returns nonzero if the caller should exit (e.g., if the widget
 * has received an Exit command), zero if not
 */
extern int DrawCanvasShouldExit(Widget w);

#endif
