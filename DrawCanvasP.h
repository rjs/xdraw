/*
 * DrawCanvas -- A widget that accepts drawing commands
 * Copyright (C) 1998-2001 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

/*
 * DrawCanvasP.h -- Private header file
 */

#ifndef _DrawCanvasP_h
#define _DrawCanvasP_h

#include "DrawCanvas-dict.h"
#include "DrawCanvas.h"
#include <X11/CoreP.h>

#define RETVAL_STR_SIZE 512 /* Maximum length of return value string */

typedef struct {
  int empty;
} DrawCanvasClassPart;

typedef struct _DrawCanvasClassRec {
  CoreClassPart core_class;
  DrawCanvasClassPart draw_class;
} DrawCanvasClassRec;

extern DrawCanvasClassRec drawCanvasClassRec;

typedef struct {
  /* Resources */

  Boolean allow_file_read, allow_file_write;
  Pixel foreground;
  XtCallbackList callback;

  /* Private state */

  GC dgc, pmgc; /* GCs for drawing and for copying areas (from pixmaps) */
  Pixmap winpm; /* The pixmap that is a buffer for our window */
  Pixmap pm; /* The pixmap we currently draw into */
  int winpmwidth, winpmheight; /* Width and height of winpm */
  int should_exit; /* DrawCanvasShouldExit returns this */
  int drawinwindow; /* (boolean) Whether to draw in our window */
  int drawinwindow_set; /* (boolean) User setting for drawinwindow */
  int processexpose; /* (boolean) Whether to process Expose events */
  int reportexpose; /* (boolean) Whether to report Expose events */
  int reportresize; /* (boolean) Whether to report Resize events */
  EventMask eventmask;
  int exit_on_event; /* (boolean) Whether to exit on next reported event */

  /* Return values */
  int retval_exists;
  char retval_str[RETVAL_STR_SIZE];

  /* GC stack: allocated size, index of first free element */
  int gc_stack_size, gc_stack_pointer;
  GC *gc_stack;

  /* Font and color caches, pixmap storage */
  Dictionary *colors, *fonts, *pixmaps;

  /*
   * For the parser:
   * parse_origcmd = the (unmodifiable!) command we are parsing
   * parse_cmd = a copy (that will be modified) of the current command
   * parse_pos = where in parse_cmd the parser is currently,
   *   or NULL if there's nothing left of the command
   * parse_allocp = a pointer to something allocated using
   *   parse_malloc(), or NULL if none
   */
  char *parse_origcmd, *parse_cmd, *parse_pos;
  void *parse_allocp;
} DrawCanvasPart;

typedef struct _DrawCanvasRec {
  CorePart core;
  DrawCanvasPart draw;
} DrawCanvasRec;

#endif
