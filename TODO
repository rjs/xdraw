Todo list for xdraw
===================

Here are features that I have planned to implement one day. It might
take me a while to do it, though... See also the BUGS file.

Features that I plan to implement in xdraw itself:

 * It is currently not possible to change the shapemasks associated
   with pixmaps from within xdraw. We would need a GC with depth 1 for
   this to work, and there is currently no provision for GCs of
   varying depth.
 * Support for reading and writing pixmaps via arguments in xdraw
   commands (using XpmCreatePixmapFromBuffer or ...FromData).
 * Support for named GCs (accessed by name rather than from a stack).
 * Support for quickly drawing an area of single differently-colored
   pixels (for drawing fractals etc.). (DrawPoint is rather slow
   mostly because of the X protocol.) This would probably be done with
   XImages and MIT-SHM, and the command syntax would be something that
   includes an array of pixel color values (or indices to some sort of
   table) as a large list. The above-mentioned support for
   XpmCreatePixmapFromBuffer might be enough for this.

Features I plan to implement in the programming language interfaces:

 * Write an interface for SCM (another Scheme implementation).
 * Write interfaces for more programming languages.

Features that I will probably not add to xdraw:

 * Support for calculations, scripting or abstraction in xdraw's
   command language. I think it's better to use another program that
   generates commands for xdraw than to add programming language
   features to xdraw's command language.
 * Support for reading image formats other than XPM. You can use other
   external programs for converting them.
