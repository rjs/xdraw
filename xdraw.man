.\" xdraw -- Command-based X11 graphics extension for programming languages
.\" Copyright (C) 1998-2001 Riku Saikkonen
.TH XDRAW 1 "xdraw version 1.40" "Riku Saikkonen" "XDRAW MANUAL"
.SH NAME
xdraw \- Command-based X11 graphics extension for programming languages

.SH SYNOPSIS
.B xdraw
[-ignorebreak] [-useviewport] [-nofileread] [-nofilewrite]
.RI [ "standard X Toolkit arguments" " ... ]"

.SH DESCRIPTION
This manual page documents
.B xdraw
version 1.40.
.PP
.B xdraw
is an extension for programming languages for drawing graphics into an
X Window System window.
.B xdraw
reads drawing commands from its standard input and draws into its X11
window.

.SH OPTIONS
.B xdraw
accepts the standard X Toolkit command-line arguments; see the
.BR X (1)
manual page for details.
.B xdraw
also accepts the following additional arguments (the corresponding X
resource settings are listed in parentheses after the arguments):
.TP
.I -ignorebreak
.RB ( "*ignoreBreak: True" )
Ignore the break signal SIGINT, instead of having it exit
.BR xdraw .
This option was added because at least the MzScheme Scheme
implementation propagates its SIGINT to child processes, and a "user
break" event should not always quit 
.BR xdraw .
.TP
.I -useviewport
.RB ( "*useViewport: True" )
Use a Viewport widget on top of the
.B xdraw
window. This normally causes scrollbars to be drawn when the window is
resized to be smaller than it was. Thus, parts of
.BR xdraw 's
drawing area never disappear when the window is resized to a smaller
size.
.B Note:
This is currently somewhat of an experimental feature. In the current
version, it is not easy to control the behaviour of the scrollbars
(when they appear and disappear), and resizing a window to be larger
using the
.B ResizeWindow
command actually resizes only the area inside the scrollbars.
.TP
.I -nofileread
.RB ( "*draw.allowFileRead: False" )
Disallow the reading of XPM files with the
.B ReadFileToPixmap
command.
.TP
.I -nofilewrite
.RB ( "*draw.allowFileWrite: False" )
Disallow the writing of XPM files with the
.BR WriteFileFromPixmap " and " WriteFileFromWindow
commands.

.SH "COMMAND SYNTAX"
The commands sent to
.B xdraw
are single-line commands, each starting at the beginning of a line,
and ending in a newline character. The commands currently consist of
symbols, integers, strings and lists. These will be explained in more
detail shortly.
.PP
The first element in a command is always a symbol: the name of the
command. After this follow possible arguments to the command,
separated by whitespace. The arguments need to be of certain types;
these are listed in the "commands" section below.
.PP
A symbol is a string of alphanumerics, starting with an alphabetic
character (as a
.BR grep (1)
regular expression,
.IR [a-zA-Z][a-zA-Z0-9]* ),
and it is always a fixed symbol looked up from a symbol table. Symbols
are case-insensitive.
.PP
A Boolean symbol is a symbol denoting a Boolean value. The symbol can
be either
.BR off " or " on .
.PP
An integer is a string of decimal numbers possibly preceded by a + or
-
.RI ( [-+]?[0-9]+ ),
interpreted as you would expect. This is converted to the C data type
.BR int .
.PP
A string is a string of characters surrounded by double quotes
.RI ( """.*""" ).
The following escapes can be used inside strings:
.B \\\\n
translates to a newline character, 
.B \\\\"\""
translates to a double quote character, and
.B \\\\\\\\
translates to one backslash.
.PP
So far, the only lists are lists of lists, arranged like
( ( element ... ) ... ). Currently, there
.I must
be whitespace around all the parentheses.

.SH COMMANDS
In the description below, the types of the arguments to the commands
are denoted as
.BR name:T ,
where T is the type of the argument:
.B Y
for a symbol,
.B B
for a Boolean symbol,
.B I
for an integer, or
.B S
for a string. Lists are denoted as
.BR "( ( el1:T el2:T ) ... )" ,
which means a list of lists with elements
.I el1
and
.I el2
inside the sublists. For example, ( ( 1 2 ) ( 10 5 ) ) is an example
of a list denoted as
.IR "( ( x:I y:I ) ... )" .
.PP
The descriptions below try not to assume familiarity with the Xlib
graphics interface. In case you still need more details on it, please
see the appropriate manual pages and the interface document referred
to in the SEE ALSO section below.
.PP
A GC (Graphics Context) is an X data structure containing information
on colors, fonts, line types and other such information. Settings in
the current GC give details on how things are drawn (for example, what
color lines are drawn in).

.SS "Special commands"
These have no direct equivalent in Xlib.
.TP
.B Reset
Frees allocated fonts and colors, sets GC parameters to defaults, and
clears the window (using the default background color).
.TP
.B ResetGC
Resets GC parameters to defaults. In practice, frees the current GC
and allocates a new one.
.TP
.B PushGC
Pushes the current GC onto a stack of GCs, and resets the GC
parameters to defaults (in practice, allocates a new GC).
.TP
.B PopGC
Pops GC parameters from the stack of GCs (in practice, frees the
current GC and gets a new one from the stack).
.TP
.BI SetWindowDrawing " commands:B expose:B"
.I commands
specifies whether drawing commands draw on the 
.B xdraw
window
.RB ( on )
or only on
.BR xdraw 's
internal buffer
.RB ( off ).
The internal buffer is a pixmap that
.B xdraw
normally uses as a copy of what should be visible in the window.
.I expose
specifies whether to process expose events (which are generated when
parts of the window become visible after being obscured) by copying
data from the internal pixmap to the window. Both are
.B on
by default (that is, before this command is invoked).
.TP
.BI RedrawWindowArea " x:I y:I width:I height:I"
Redraw the given area of the window, copying the data from
.BR xdraw 's
internal pixmap. This is usually used to redraw the window manually
when automatic drawing is disabled with the
.B SetWindowDrawing
command.
.TP
.BI ResizeWindow " width:I height:I"
Resize the
.B xdraw
window to the given size.
.TP
.B Exit
Closes the window and exits
.B xdraw
cleanly.

.SS "GC manipulation commands"
These manipulate the current GC (Graphics Context), and thus affect
all future drawing commands.
.PP
All of these functions call the standard Xlib function with the same
name (except with an
.B X
prepended). See the commands' manual pages and the
.BR XGCValues (3x)
manual page for details.
.TP
.BI SetForeground " foreground:S"
Sets the current foreground color to
.IR foreground .
The color can be any standard X11 color name (see the
.BR X (1)
manual page). This command allocates the named color into
.BR xdraw 's
internal color cache. The default color is taken from the resource
database (and is not stored in the cache).
.TP
.BI SetBackground " background:S"
Sets the current background color to
.IR background .
The color can be any standard X11 color name (see the
.BR X (1)
manual page). This command allocates the named color into
.BR xdraw 's
internal color cache. The default color is taken from the resource
database (and is not stored in the cache).
.TP
.BI SetFunction " function:Y"
Sets the current GC function. The available functions are:
.BR clear ", " and ", " andReverse ,
.BR copy ", " andInverted ", " noop ,
.BR xor ", " or ", " nor ,
.BR equiv ", " invert ", " orReverse ,
.BR copyInverted ", " orInverted " and " nand .
The default is
.BR copy .
See the
.BR XGCValues (3x)
manual page for details.
.TP
.BI SetLineAttributes " width:I line_style:Y cap_style:Y join_style:Y"
Sets the attributes of all lines drawn (including those in polygons
etc.).
.I width
is the width of the lines, in pixels (default 0). As a special case, a
.I width
of 0 pixels draws a 1-pixel-wide line using a fast algorithm; the
result might not be exactly the same on all X servers.
.I line_style
sets the style of the lines, and can be
.BR Solid ", " OnOffDash " or " DoubleDash .
.I cap_style
sets the style of the ends of lines, and can be
.BR NotLast ", " Butt ", " Round " or"
.BR Projecting .
.I join_style
specifies how to join connected lines (e.g., those that are drawn to
produce a polygon), and can be
.BR Miter ", " Round " or " Bevel .
The defaults are
.BR Solid ", " Butt " and " Miter ,
respectively; try the different choices on wide lines to see what
effect they have.
.TP
.BI SetDashes " dash_offset:I ( ( length:I ) ... )"
Sets the current dash list (for drawing dashed lines) to the given
list. Every other number in the lists is the length (in pixels) of a
dash, and the rest specify the lengths of the empty spaces between
dashes. See the
.BR XSetDashes (3x)
manual page for details.
.TP
.BI SetFillRule " fill_rule:Y"
Sets the current fill rule, either
.B EvenOdd
(the default) or
.BR Winding .
This only affects how the interiors of self-intersecting polygons are
filled.
.TP
.BI SetFont " font:S"
Sets the current font to
.IR font .
.I font
can be any X11 font specification (see the
.BR X (1)
manual page). This command allocates the named font into
.BR xdraw 's
internal font cache. The default font is unspecified (by the X
Toolkit).
.TP
.BI SetClipRectangles " clip_x_origin:I clip_y_origin:I ( ( x:Y y:I width:I height:I ) ... ) ordering:Y"
Sets the current clip mask to the union of the specified rectangles.
With this command, you can specify areas that future drawing commands
will not draw in.
.I ordering
can be
.BR Unsorted ", " YSorted ", " YXSorted " or"
.BR YXBanded .
See the
.BR XSetClipRectangles (3x)
manual page for details. The default is to have no clipping (drawing
commands can draw everywhere in the window).
.TP
.BI SetClipOrigin " clip_x_origin:I clip_y_origin:I"
Sets the current clip origin to the specified coordinates.
.TP
.B ClearClipMask
Clears the clip mask currently in effect. After this command, drawing
commands draw to the whole window again.
.TP
.BI SetArcMode " arc_mode:Y"
Sets the mode for filling arcs, either
.B Chord
or
.B PieSlice 
(the default).

.SS "Drawing commands"
These draw into the
.B xdraw
window (and to an internal pixmap which is used for redrawing the
window when necessary) using the current GC.
.PP
All of these functions call the standard Xlib function with the same
name (except with an
.B X
prepended). See the commands' manual pages for details.
.TP
.BI CopyArea " src_x:I src_y:I width:I height:I dest_x:I dest_y:I"
Copies a rectangular area of the window from one place to another.
The current GC function (set by the
.B SetFunction
command) affects this.
.RI ( src_x ", " src_y )
and
.RI ( dest_x ", " dest_y )
are the upper left corners of the source and destination rectangles.
.TP
.BI DrawPoint " x:I y:I"
Draws a single point at the given coordinates.
.TP
.BI DrawPoints " ( ( x:I y:I ) ... ) mode:Y"
Draws multiple points.
.I mode
can be either
.B Origin
(coordinates are specified relative to the origin of the window) or
.B Previous
(the first coordinates are relative to the origin of the window, and
each of the subsequent coordinates are relative to the preceding
ones).
.TP
.BI DrawLine " x1:I y1:I x2:I y2:I"
Draws a single line from
.RI ( x1 ", " y1 )
to 
.RI ( x2 ", " y2 ).
.TP
.BI DrawLines " ( ( x:I y:I ) ... ) mode:Y"
Draws multiple connected lines (joining the given coordinates).
.I mode
is as in
.BR DrawPoints .
.TP
.BI DrawSegments " ( ( x1:I y1:I x2:I y2:I ) ... )"
Draws multiple unconnected lines. Coordinates are relative to the
origin of the window.
.TP
.BI DrawRectangle " x:I y:I width:I height:I"
Draws a single unfilled rectangle.
.RI ( x ", " y )
is the upper left corner of the rectangle. Because of how Xlib
specifies this function, this actually draws the outline of a
rectangle
.I width + 1
pixels wide and
.I height + 1
pixels high.
.TP
.BI DrawRectangles " ( ( x:I y:I width:I height:I ) ... )"
Draws multiple unfilled rectangles.
.TP
.BI DrawArc " x:I y:I width:I height:I angle1:I angle2:I"
Draws an arc (or ellipse or circle). See the
.BR XDrawArc (3x)
manual page for details on the parameters.
.TP
.BI DrawArcs " ( ( x:I y:I width:I height:I angle1:I angle2:I ) ... )"
Draws multiple arcs.
.TP
.BI FillRectangle " x:I y:I width:I height:I"
Draws a filled rectangle.
.IR width " and " height
are the exact width and height, in pixels, of the filled area.
.TP
.BI FillRectangles " ( ( x:I y:I width:I height:I ) ... )"
Draws multiple filled rectangles.
.TP
.BI FillPolygon " ( ( x:I y:I ) ... ) shape:Y mode:Y"
Draws a filled polygon using the given coordinates for its corners.
.I shape
can be
.BR Complex ", " Convex " or"
.BR Nonconvex .
This is just used for optimisation; if you don't care how quickly the
polygon is drawn, you can always use
.BR Complex .
.I mode
is as in
.BR DrawPoint .
See the
.BR XFillPolygon (3x)
manual page for details.
.TP
.BI FillArc " x:I y:I width:I height:I angle1:I angle2:I"
Draws a filled arc. See the
.BR XFillArc (3x)
manual page for details on the parameters.
.TP
.BI FillArcs " ( ( x:I y:I width:I height:I angle1:I angle2:I ) ... )"
Draws multiple filled arcs.
.TP
.BI DrawString " x:I y:I string:S"
Draws the text string
.I string
using the current font.
.RI ( x ", " y )
specifies the location of the left corner of the baseline of the text
(the baseline is a horizontal line located at the bottom of a
character like 
.BR m ).
The background of the text is kept as is.
.TP
.BI DrawImageString " x:I y:I string:S"
Draws the text string
.I string
using the current font. 
.RI ( x ", " y )
specifies the location of the left corner of the baseline of the text.
The background of the text is cleared to the current background color.

.SS "Special drawing commands"
These draw to the window like the normal drawing commands, but these
have no direct counterpart in Xlib.
.TP
.BI DrawRoundedRectangle " x:I y:I w:I h:I ew:I eh:I"
Draws a rounded rectangle using
.BR XmuDrawRoundedRectangle .
.IR ew " and " eh
are the width and height of a bounding box for the rounded corners.
.TP
.BI FillRoundedRectangle " x:I y:I w:I h:I ew:I eh:I"
Draws a filled rounded rectangle using
.BR XmuFillRoundedRectangle .
.IR ew " and " eh
are as in
.BR DrawRoundedRectangle .
.TP
.BI DrawJustifiedString " x:I y:I hjust:Y vjust:Y type:Y string:S"
Draws the text string
.I string
using the current font and with the given justification.
.I hjust
specifies the position of the given
.I x
coordinate relative to the text; it can be
.BR Left ", " Center " or " Right .
.I vjust
specifies the position of the given
.I y
coordinate relative to the text; it can be
.BR Baseline ", " Top ", " Center " or"
.BR Bottom .
.I type
is either
.BR Normal " or " Image ,
and specifies whether to draw the string using
.BR XDrawString (3x)
or using
.BR XDrawImageString (3x).
(The difference is that the latter one clears the background of the
text.)

.SS Input commands
These commands can be used to get information from the 
.B xdraw
process. All of these commands return a value; this is returned by
writing a line of the form
.BI "( return " "values ..." " )"
to the standard output.
.TP
.B GetWindowSize
Returns the current size of
.BR xdraw 's
window. The return value is of the form
.BI "( return " "width:I height:I"
.BR ) .
.TP
.B GetxdrawVersion
Returns the version of the running copy of
.BR xdraw ,
in the form
.BI "( return " version:S
.BR ) .
.TP
.BI GetTextExtents " string:S"
Returns the extents (i.e., size in pixels) of the given
.I string
using the current font. The return value is of the form
.BI "( return " "direction:Y font-ascent:I font-descent:I"
.I "lbearing:I rbearing:I width:I ascent:I descent:I"
.BR ) ,
where
.I direction
is either
.BR LeftToRight " or " RightToLeft ,
.IR font-ascent " and " font-descent
give the maximum ascent and descent of the current font, and the rest
of the values give the extents of the given string. See the
.BR XQueryTextExtents (3x)
manual page for details on the values.
.TP
.B GetPointerPosition
Returns the current position of the pointer (the mouse cursor), in the
form
.BI "( return True " "x:I y:I " ( " stateflag:Y ... " )
.BR ) ,
where
.IR x " and " y
are the current coordinates of the mouse cursor (relative to the upper
left corner of
.BR xdraw 's
window). See the description of
.B ReportKeyEvents
below for details on the
.IR stateflag s.
If the pointer is not on the same screen as the 
.B xdraw
window,
.B GetPointerPosition
returns
.BR "( return False -1 -1 ( ) )" .

.SS Event reporting commands
These commands turn on or off the reporting of various kinds of
X events, which occur when various things happen in the
.B xdraw
window. All reporting is off by default (that is, before these
commands are invoked).
.B xdraw
reports events by writing lines of the form
.BI "( event " "name:Y parameters ..." " )"
to the standard output. Events and return values always appear on
separate lines, but
.B xdraw
can report an event before it processes a command it has received.
This means that you should be ready to receive an event even when
waiting for a return value from a command you sent.
.TP
.BI ReportExposeEvents " state:B"
Turns the reporting of Expose events on or off, according to
.IR state .
Expose events are usually generated when a previously obscured part of
the window becomes visible and needs to be redrawn. Expose events are
reported in the form
.BI "( event Expose " "x:I y:I width:I height:I count:I"
.BR ) ,
where
.IR x " and " y
give the upper-left corner of a rectangle to be redrawn, and
.IR width " and " height
give the size of the rectangle. If
.I count
is nonzero, at least that many Expose events will follow the current
one;
.B xdraw
does not compress expose events. See the
.BR XExposeEvent (3x)
manual page for details.
.TP
.BI ReportResizeEvents " state:B"
Turns the reporting of resize (ConfigureNotify) events on or off,
according to
.IR state .
These events are generated when
.BR xdraw 's
window is resized. The events are reported in the form
.BI "( event Resize " "width:I height:I"
.BR ) ,
where
.IR width " and " height
give the new size of
.BR xdraw 's
window.
.TP
.BI ReportKeyEvents " press:B release:B"
Turns the reporting of keyboard events on or off.
.IR press " and " release
specify whether to report
.BR KeyPress " and " KeyRelease
events, respectively.
.B KeyPress
events are generated when a keyboard key is pressed, and when it is
held down long enough for the key to repeat (in which case events are
generated repeatedly, at the key repeat rate of the X server).
.B KeyRelease
events are generated when a keyboard key is released (note that some
keyboards may not be able to generate
.B KeyRelease
events). Keyboard events are generated only when the
.B xdraw
window has the input focus, which is usually controlled by the window
manager.

Keyboard events are reported in the form
.BI "( event " "type:Y x:I y:I " ( " stateflag:Y ... " ) " keysym:S"
.BR ) ,
where
.I type
is either
.BR KeyPress " or " KeyRelease ,
and
.IR x " and " y
give the coordinates of the mouse cursor (relative to the upper left
corner of
.BR xdraw 's
window) at the time of the event.
.I keysym
is a string giving the symbolic name of the key that was pressed. The
possible names are listed in
.IR /usr/include/X11/keysymdef.h ;
remove the
.B XK_
prefix from the symbols defined there to get the names.

The list of
.IR stateflag s
reports the state of certain key and button "modifiers" at the time of
the event. The list can contain the following symbols:
.BR Button1 ", " Button2 ", " Button3 ,
.BR Button4 ", " Button5 ", " Control ,
.BR Lock ", " Mod1 ", " Mod2 ,
.BR Mod3 ", " Mod4 ", " Mod5 " and"
.BR Shift .
The modifiers specify which mouse buttons and modifier keys (e.g., the
Shift key) are being held down.
.B Lock
is in the list if "Caps Lock" is on (even if the key is not held
down); some of the system-specific modifiers in
.BI Mod n
can also be locks of this kind. Note that the list of state flags can
also be empty, i.e., 
.BR "( )" .
.TP
.BI ReportButtonEvents " press:B release:B"
Turns the reporting of mouse button events on or off.
.IR press " and " release
specify whether to report
.BR ButtonPress " and " ButtonRelease
events, respectively. These events are generated whenever mouse
buttons are pressed or released while the mouse is inside
.BR xdraw 's
window. The events are reported in the form
.BI "( event " "type:Y x:I y:I " ( " stateflag:Y ... " ) " button:I"
.BR ) ,
where
.I type
is either
.BR ButtonPress " or " ButtonRelease ,
and
.I button
gives the number of the mouse button (1 to 5) that was pressed. Mouse
buttons are usually numbered from left to right. See the description
of
.B ReportKeyEvents
above for details on the other parameters.
.TP
.BI ReportEnterLeaveEvents " enter:B leave:B"
Turns the reporting of EnterNotify and LeaveNotify events on or off.
.IR enter " and " leave
specify whether to report
.BR EnterNotify " and " LeaveNotify
events, respectively. These events are generated when the mouse cursor
leaves or enters
.BR xdraw 's
window. The events are reported in the form
.BI "( event " "type:Y focus:Y " ( " stateflag:Y ..."
.BR ") )" ,
where
.I type
is either
.BR EnterNotify " or " LeaveNotify .
.I focus
is either
.BR True " or " False
and specifies whether
.BR xdraw 's
window had input focus at the time of the event (the relative ordering
of these events and focus change events is unspecified). See the
description of
.B ReportKeyEvents
above for details on the
.IR stateflag s.
See the
.BR XCrossingEvent (3x)
manual page for some details on these events.
.TP
.BI ReportFocusChangeEvents " state:B"
Turns the reporting of FocusIn and FocusOut events on or off,
according to
.I state
(the two event types are turned on or off together). FocusIn and
FocusOut events are generated when the
.B xdraw
window receives or loses input focus, respectively. Keys pressed on
the keyboard usually go to the window with input focus; typically the
window manager displays this window with a differently colored border.
The events are reported in the form
.BI "( event " type:Y
.BR ) ,
where
.I type
is either
.BR FocusIn " or " FocusOut .
.TP
.BI ReportMotionEvents " motion:Y"
Controls the reporting of MotionNotify events, generated whenever the
pointer (usually the mouse) is moved.
.I motion
can be
.B off
to disable all pointer motion events,
.B on
to enable them, or
.B hint
to enable only pointer motion hint events (the event mask
.BR PointerMotionHintMask ).

Enabling motion hint events gives you only one event when the pointer
moves, and you need to use
.B GetPointerPosition
to get the true current position of the mouse. If the pointer is moved
after a
.BR GetPointerPosition ,
you get another motion hint event. The purpose of motion hint events
is simply to reduce the amount of events generated; if you enable all
motion events, you may get so many that processing them takes too much
time.

The events are reported in the form
.BI "( event MotionNotify " "x:I y:I " ( " stateflag:Y ... " ) " hint:Y"
.BR ) ,
where
.IR x " and " y
give the coordinates of the mouse cursor (relative to the upper left
corner of
.BR xdraw 's
window) at the time of the event. See the description of
.B ReportKeyEvents
above for details on the
.IR stateflag s.
.B hint
is either
.BR Hint " or " Normal ,
and specifies whether this is a motion hint event. Please note that
the
.I x " and " y
coordinates are not current for motion hint events; always use
.B GetPointerPosition
to get the current position when using motion hint events.
.TP
.BI ExitOnEvent " state:B"
If
.I state
is true, switches into a special mode where, instead of reporting an
event,
.B xdraw
exits. Also, in this mode, an end-of-file on
.BR xdraw 's
standard input does not exit
.BR xdraw .
If
.I state
is false, leaves this special mode, causing event reporting to occur
normally.

This can be used, for example, to give a bunch of commands to 
.B xdraw
from a file and let the user close the
.B xdraw
window automatically by doing something in it. If the file contains
the commands
.B "ExitOnEvent on"
and
.BR "ReportButtonEvents on off" ", " xdraw
will not automatically exit when the file ends, but will wait for the
user to press a mouse button in the
.B xdraw
window before exiting.

.SS Pixmap-related commands
These commands provide an interface to pixmaps and the XPM library.
.B xdraw
maintains storage for an unlimited number of pixmaps, indexed by a
string (later called
.IR pixmapname ).
A pixmap is basically an offscreen storage for graphics data; you can
draw to it and copy data from it to the
.B xdraw
window.

Each pixmap is automatically associated with a shapemask. The
shapemask is a bitmap containing a zero bit in each position where the
pixmap has a transparent color. XPM files have a concept of a
transparent color; pixels containing the transparent color in the XPM
file become zero bits in the shapemask. To use transparent colors when
drawing, see the
.BR SetClipMask " and " SetClipOrigin
commands. Please note that it is not currently possible to modify the
shapemask from within
.BR xdraw .
.TP
.BI CreatePixmap " pixmapname:S width:I height:I"
Creates a new pixmap with the given name and size. The pixmap is
cleared to the background color. The associated shapemask is
initialised to all ones, so nothing is clipped if it is used.
.TP
.BI FreePixmap " pixmapname:S"
Frees the storage associated with a previously created pixmap.
.TP
.B FreePixmaps
Frees all previously created pixmaps.
.TP
.BI SetDrawTargetToPixmap " pixmapname:S"
After this command, all drawing commands draw to the named pixmap
instead of to the
.B xdraw
window.
.TP
.B SetDrawTargetToWindow
Cancels a previous
.B SetDrawTargetToPixmap
command. After this command, drawing commands draw to the
.B xdraw
window (and/or
.BR xdraw 's
internal buffer, according to the setting in
.BR SetWindowDrawing ).
.TP
.BI SetClipMask " pixmapname:S"
Sets the current clip mask to the shapemask associated with the named
pixmap. The
.BR ClearClipMask " and " SetClipRectangles
commands reset this setting.
.TP
.BI CopyAreaFromPixmap " src_pixmapname:S src_x:I src_y:I width:I height:I dest_x:I dest_y:I"
Copies data from the named pixmap to the current draw target (the
.B xdraw
window or another pixmap if the
.B SetDrawTargetToPixmap
command is in effect). This command is implemented using the function
.BR XCopyArea (3x).

If you want the copying to respect a possible transparent color in the
pixmap, see the documentation of the
.B CopyFromPixmap
command below. Use the command sequence mentioned there if you need
finer control over the copying process.
.TP
.BI CopyFromPixmap " srcpixmapname:S dest_x:I dest_y:I"
Like 
.B CopyAreaFromPixmap
but copies the whole pixmap to the specified position in the draw
target, and handles transparent colors automatically.

Conceptually, this command is a shorthand for the sequence
.BR PushGC ,
.B SetClipMask
.IR "srcpixmapname" ,
.B SetClipOrigin
.IR "dest_x dest_y" ,
.B CopyAreaFromPixmap
.IR "srcpixmapname 0 0 width height dest_x dest_y" ,
.B PopGC ,
where
.IR width " and " height
are taken from the size of the pixmap (queried from the X server).
Actually, this command does not create a new GC but reuses a
predefined one. Please note that
.BR xdraw 's
GC manipulation commands do not affect this command!

This command is intended to be a simple way to use a pixmap containing
transparent colors.
.TP
.BI GetPixmapSize " pixmapname:S"
Returns the size of the named pixmap. The return value is of the form
.BI "( return " "width:I height:I"
.BR ) .
.TP
.BI ReadFileToPixmap " filename:S pixmapname:S"
Creates a new pixmap whose contents are read from the named file. The
file needs to be in XPM (X PixMap) format. This command is an
interface to the function
.BR XpmReadFileToPixmap ()
in the XPM library.
.TP
.BI WriteFileFromPixmap " filename:S pixmapname:S"
Dumps the contents of the named pixmap to a newly-created file, in
XPM format. This command is an interface to the function
.BR XpmWriteFileFromPixmap ()
in the XPM library.
.TP
.BI WriteFileFromWindow " filename:S"
Dumps the current contents of the
.B xdraw
window to a newly-created file, in
XPM format. This command is implemented using the function
.BR XpmWriteFileFromPixmap ()
in the XPM library.

.SH ERRORS
.B xdraw
reports errors to its standard error stream. For parsing errors, it
displays the command it got and shows where the parse error occured.
Xlib may display additional errors (for example, if some given
arguments are invalid); these are also displayed on the standard error
stream.

.SH RESOURCES
.B xdraw
does not require an application defaults file, although one is supplied
by default in case you need to customise it. The application defaults
file should be named 
.BR XDraw .

.B xdraw
supports the standard X Toolkit resources. For example, the
.B geometry
resource specifies the initial size of the window (the default is
200x200 pixels), and the
.B background
and
.B foreground
resources specify the initial colors of the window.

.BR xdraw 's
widget hierarchy is simple: There is a custom
.B DrawCanvas
widget (subclassed from the X Toolkit
.B Core
widget) as the only child of the application shell.

Useful additional X resources supported by
.B xdraw
are:
.TP
.B "*ignoreBreak"
(class 
.BR IgnoreBreak )
Set this to
.B true
to ignore SIGINT. See the command-line option
.B -ignorebreak
above. The default is
.BR false .
.TP
.B "*draw.allowFileRead"
(classes
.BR DrawCanvas.AllowFileAccess )
Set this to
.B false
to disallow the reading of files. See the command-line option
.B -nofileread
above. The default is
.BR true .
.TP
.B "*draw.allowFileWrite"
(classes
.BR DrawCanvas.AllowFileAccess )
Set this to
.B false
to disallow the writing of files. See the command-line option
.B -nofilewrite
above. The default is
.BR true .

.SH EXAMPLE
Here are some examples of the commands that
.B xdraw
accepts.
.PP
Reset
.br
SetForeground "blue"
.br
SetFont "fixed"
.br
DrawString 50 50 "Hello, world!"
.br
DrawLine 0 55 200 55
.br
DrawRectangles ( ( 0 0 200 200 ) ( 5 5 190 190 ) )
.br
FillPolygon ( ( 10 10 ) ( 190 10 ) ( 190 10 ) ( 190 190 ) ) Complex Origin

.SH "SEE ALSO"
.BR X (1),
.BR XGCValues (3x),
.BR XSetForeground (3x),
.BR XSetBackground (3x),
.BR XSetFunction (3x),
.BR XSetLineAttributes (3x),
.BR XSetDashes (3x),
.BR XSetFillRule (3x),
.BR XSetFont (3x),
.BR XSetClipRectangles (3x),
.BR XSetClipOrigin (3x),
.BR XSetArcMode (3x),
.BR XCopyArea (3x),
.BR XDrawPoint (3x),
.BR XDrawPoints (3x),
.BR XDrawLine (3x),
.BR XDrawLines (3x),
.BR XDrawSegments (3x),
.BR XDrawRectangle (3x),
.BR XDrawRectangles (3x),
.BR XDrawArc (3x),
.BR XDrawArcs (3x),
.BR XFillRectangle (3x),
.BR XFillRectangles (3x),
.BR XFillPolygon (3x),
.BR XFillArc (3x),
.BR XFillArcs (3x),
.BR XDrawString (3x),
.BR XDrawImageString (3x),
.BR XQueryTextExtents (3x),
.BR XCreatePixmap (3x),
.BR XFreePixmap (3x),
.BR XSetClipMask (3x),
.br
.IR "Xlib \- C Language X Interface" ,
.br
.I Xmu Library
(manual),
.br
.I XPM Manual

.SH COPYRIGHT
.B xdraw
\- Command-based X11 graphics extension for programming languages
.br
Copyright (C) 1998-2001 Riku Saikkonen
.PP
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
.PP
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
.PP
You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
.PP
Author: Riku Saikkonen <rjs@netti.fi>
