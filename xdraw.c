/*
 * xdraw -- Command-based X11 graphics extension for programming languages
 * Copyright (C) 1998-2001 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

/*
 * xdraw.c -- Main program
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/Xaw/Viewport.h>
#include "DrawCanvas.h"
#include "version.h"

/* Application resources */

struct myresources {
  Boolean ignbreak;
  Boolean useviewport;
} my_res;

#define rs(field) (sizeof(my_res.field))
#define ro(field) (XtOffsetOf(struct myresources,field))
static XtResource my_resource_list[] =
{
  {"ignoreBreak", "IgnoreBreak", XtRBoolean, rs(ignbreak), ro(ignbreak),
   XtRImmediate, (XtPointer) False},
  {"useViewport", "UseViewport", XtRBoolean, rs(useviewport),
   ro(useviewport), XtRImmediate, (XtPointer) False}
};
#undef ro
#undef rs

static XrmOptionDescRec cmdlineres[] =
{
  {"-ignorebreak", "*ignoreBreak", XrmoptionNoArg, "True"},
  {"-useviewport", "*useViewport", XrmoptionNoArg, "True"},
  {"-nofileread", "*draw.allowFileRead", XrmoptionNoArg, "False"},
  {"-nofilewrite", "*draw.allowFileWrite", XrmoptionNoArg, "False"}
};

/* Initial size of input buffer */
#define INIT_BUF_SIZE 512

XtAppContext app_con;
Widget top, viewport, draw;
XtInputId stdin_input_id;

/* inbuf[inbuf_pos] is the first unused character in inbuf */
char *inbuf;
int inbuf_size, inbuf_pos;

/* 
 * Search for ch from ptr to ptr+len (inclusive, so ptr[len] is the
 * last character searched).
 * Returns a pointer to the first instance of ch, or NULL if it didn't
 * find any. 
 */
static char *find_char(char ch, const char *ptr, int len)
{
  int i;
  char *cp;

  for (cp = (char *) ptr, i = 0; i < len; i++, cp++)
    if (*cp == ch) return cp;

  return NULL;
}

/* This callback is called when we get something from stdin */
static void cb_stdin(XtPointer closure, int *source, XtInputId *id)
{
  ssize_t want_to_read, num_read, next_size;
  char *nl_ptr, *retstr;

  do
  {
    if (inbuf_pos == inbuf_size)
    {
      /* inbuf is full, so allocate more space for it */
      inbuf_size *= 2;
      inbuf = XtRealloc(inbuf, inbuf_size * sizeof(char)); 
    }

    want_to_read = inbuf_size - inbuf_pos;
    /* read(2) is undefined if count > SSIZE_MAX */
    if (want_to_read > SSIZE_MAX)
      want_to_read = SSIZE_MAX;

    /* This doesn't block, because we are in nonblocking mode */
    num_read = read(*source, &inbuf[inbuf_pos], want_to_read);

    if (num_read < 0) /* Read returned an error */
    {
      if (errno == EINTR || errno == EAGAIN)
	return; /* We're ok, but we got no data, so break out */

      /* (XXX) This should be done with one call to XtAppError... */
      XtAppWarning(app_con, "xdraw: read from stdin returned an error.");
      XtAppError(app_con, strerror(errno));
    }

    if (num_read == 0) /* End of file */
    {
      /* An internal command to notify the widget of EOF */
      DrawCanvasCommand(draw, "EndOfFile");

      if (DrawCanvasShouldExit(draw)) {
	XtDestroyApplicationContext(app_con);
	exit(0);
      } else {
	XtRemoveInput(stdin_input_id);
	return;
      }
    }

    inbuf_pos += num_read;
    
    while (inbuf_pos > 0) /* While there are read characters... */
    {
      if ((nl_ptr = find_char('\n', inbuf, inbuf_pos)) == NULL)
	break; /* No full lines in inbuf */
      else
      {
	/* We found a newline, so send off the command preceding it */
	
	*nl_ptr = 0; /* Remove the newline */
	
	/* Send the command to the DrawCanvas widget */
	retstr = DrawCanvasCommand(draw, inbuf);

	if (retstr != NULL)
	{
	  /* There is a return value; print it */
	  printf("( return %s )\n", retstr);
	  fflush(stdout);
	}

	if (DrawCanvasShouldExit(draw))
	{
	  /* We got an Exit command, so exit */
	  XtDestroyApplicationContext(app_con);
	  exit(0);
	}
	
	/*
	 * Move the next command (after the newline we found), if any,
	 * to the start of inbuf, and set inbuf_pos
	 */
	next_size = &inbuf[inbuf_pos - 1] - nl_ptr;
	if (next_size > 0)
	  memmove(inbuf, nl_ptr + 1, next_size);
	inbuf_pos = next_size;
      }
    }
  } while(1); /* Keep on reading as long as we get something */
}

/*
 * This callback is called from the widget when an event happens and
 * event reporting for that event is turned on.
 */
void cb_event(Widget w, XtPointer client_data, XtPointer call_data)
{
  /* Exit before reporting the event if we should exit */
  if (DrawCanvasShouldExit(w))
  {
    XtDestroyApplicationContext(app_con);
    exit(0);
  }

  printf("( event %s )\n", (char *) call_data);
  fflush(stdout);
}

/* The main program */
int main(int argc, char *argv[])
{
  static String fallback_resources[] = {
    "*input: true",
    "*allowShellResize: true",
    "*viewport.allowHoriz: true",
    "*viewport.allowVert: true",
    NULL
  };

  int flags;

  XtSetLanguageProc(NULL, NULL, NULL);
  
  top = XtOpenApplication(&app_con, "XDraw",
    cmdlineres, XtNumber(cmdlineres), &argc, argv, fallback_resources,
    sessionShellWidgetClass, NULL, 0);

  if (argc > 1)
  {
    /* Unprocessed arguments left */

    fprintf(stderr,
      "xdraw version " XDRAW_VERSION "\n"
      "xdraw -- Command-based X11 graphics extension for programming languages\n"
      "Copyright (C) 1998-2001 Riku Saikkonen\n"
      "\n"
      "This program is free software; you can redistribute it and/or modify\n"
      "it under the terms of the GNU General Public License as published by\n"
      "the Free Software Foundation; either version 2 of the License, or\n"
      "(at your option) any later version.\n"
      "\n"
      "This program is distributed in the hope that it will be useful,\n"
      "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
      "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
      "GNU General Public License for more details.\n"
      "\n"
      "You should have received a copy of the GNU General Public License along\n"
      "with this program; if not, write to the Free Software Foundation, Inc.,\n"
      "51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.\n"
      "\n"
      "Author: Riku Saikkonen <rjs@netti.fi>\n"
      "\n");

    fprintf(stderr,
      "Syntax: xdraw [-ignorebreak] [-useviewport] [-nofileread] [-nofilewrite]\n"
      "              [standard X Toolkit arguments ...]\n"
      "\n");

    XtAppError(app_con, "Invalid command-line arguments!");
  }

  XtGetApplicationResources(top, &my_res,
    my_resource_list, XtNumber(my_resource_list), NULL, 0);

  if (my_res.ignbreak)
    signal(SIGINT, SIG_IGN);

  if (my_res.useviewport) {
    viewport = XtCreateManagedWidget("viewport", viewportWidgetClass,
      top, NULL, 0);
    draw = XtCreateManagedWidget("draw", drawCanvasWidgetClass,
      viewport, NULL, 0);
  } else {
    draw = XtCreateManagedWidget("draw", drawCanvasWidgetClass,
      top, NULL, 0);
  }

  XtAddCallback(draw, XtNcallback, cb_event, NULL);

  XtRealizeWidget(top);

  inbuf_size = INIT_BUF_SIZE;
  inbuf_pos = 0;
  inbuf = XtMalloc(inbuf_size * sizeof(char));

  /* Set stdin to non-blocking mode */
  flags = fcntl(STDIN_FILENO, F_GETFL);
  flags |= O_NONBLOCK;
  fcntl(STDIN_FILENO, F_SETFL, flags);

  stdin_input_id = XtAppAddInput(app_con, STDIN_FILENO,
    (XtPointer) (XtInputReadMask | XtInputExceptMask), cb_stdin, NULL);

#if 1
  XtAppMainLoop(app_con);
#else /* XXX Debug code for testing event dispatch */
  {
    XEvent xe;

    for (;;) {
      XtAppNextEvent(app_con, &xe);

      if (xe.type == FocusOut) printf("Event: FocusOut\n");
      else if (xe.type == KeyPress) printf("Event: KeyPress\n");
      else printf("Event: type=%d\n", xe.type);

      if (!XtDispatchEvent(&xe))
        printf("Event not dispatched: type=%d\n", xe.type);
    }
  }
#endif
  exit(0);
}
