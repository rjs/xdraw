/*
 * DrawCanvas -- A widget that accepts drawing commands
 * Copyright (C) 1998-2001 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

/*
 * DrawCanvas-dict.c -- Dictionary data structure implementation
 *
 * The implementation is currently just an unordered, growing array. A
 * hash table would probably be faster, but this seems to be fast
 * enough.
 */

#include "DrawCanvas-dict.h"

#define INIT_ALLOC 20
#define ALLOC_INCREMENT 50
#define ALLOC_DECREMENT 300

/* Find key and return its index in the tables */
static int dict_find_index(Dictionary *dict, const char *key)
{
  int i;

  for (i = 0; i < dict->used; i++)
    if (strcmp(key, dict->keys[i]) == 0)
      return i; /* Found */

  return -1; /* Not found */
}

/* Reallocate the tables to the given size */
static void dict_realloc(Dictionary *dict, int newsize)
{
  dict->keys = (char **) XtRealloc((char *) dict->keys,
    newsize * sizeof(char *));
  dict->data = (DictData *) XtRealloc((char *) dict->data,
    newsize * sizeof(DictData));
  dict->allocated = newsize;
}

/* Swap two elements in the tables */
static void dict_swapelems(Dictionary *dict, int index1, int index2)
{
  char *tmpkey;
  DictData tmpdata;

  if (index1 == index2)
    return; /* Ignore swap with itself */

  tmpkey = dict->keys[index1];
  dict->keys[index1] = dict->keys[index2];
  dict->keys[index2] = tmpkey;

  tmpdata = dict->data[index1];
  dict->data[index1] = dict->data[index2];
  dict->data[index2] = tmpdata;
}

/* Create a new dictionary */
Dictionary *dict_create(void)
{
  Dictionary *dict = (Dictionary *) XtMalloc(sizeof(Dictionary));

  dict->allocated = INIT_ALLOC;
  dict->keys = (char **) XtMalloc(dict->allocated * sizeof(char *));
  dict->data = (DictData *) XtMalloc(dict->allocated * sizeof(DictData));
  dict->used = 0;

  return dict;
}

/* Free a dictionary */
void dict_destroy(Dictionary *dict)
{
  int i;

  if (dict == NULL)
    return;

  for (i = 0; i < dict->used; i++)
    XtFree(dict->keys[i]);

  XtFree((char *) dict->keys);
  XtFree((char *) dict->data);
  XtFree((char *) dict);
}

/* Find the data belonging to key; returns a pointer to the data or NULL */
DictData *dict_find(Dictionary *dict, const char *key)
{
  int i = dict_find_index(dict, key);

  return (i < 0) ? NULL : &dict->data[i];
}

/* Remove the element with the given key from the dictionary */
void dict_remove(Dictionary *dict, const char *key)
{
  int i = dict_find_index(dict, key);

  if (i < 0) /* Key not found */
    return;

  /* Move removed element to end of array */
  dict_swapelems(dict, i, dict->used - 1);

  XtFree(dict->keys[dict->used - 1]);

  dict->used--;
  if (dict->allocated - dict->used > ALLOC_DECREMENT)
    dict_realloc(dict, dict->allocated - ALLOC_DECREMENT);
}

/* Insert an element into the dictionary. Makes a copy of key and data. */
void dict_insert(Dictionary *dict, const char *key, DictData data)
{
  dict->used++;
  if (dict->used > dict->allocated)
    dict_realloc(dict, dict->allocated + ALLOC_INCREMENT);

  dict->keys[dict->used - 1] = XtNewString(key);
  dict->data[dict->used - 1] = data; /* copies data */
}

/* Is the dictionary empty? */
int dict_emptyp(Dictionary *dict)
{
  return (dict->used == 0);
}

/*
 * Remove any element from a non-empty dictionary and return its data.
 * Meant for emptying a dictionary, so doesn't try to reallocate it to
 * a smaller size.
 */
DictData dict_remove_any(Dictionary *dict)
{
  DictData dummy;

  if (dict->used == 0) /* Shouldn't happen */
  {
    dummy.ul = 0;
    return dummy;
  }

  dict->used--;
  XtFree(dict->keys[dict->used]);
  return dict->data[dict->used]; /* data is copied here */
}
