xdraw change log  -*- text -*-
================

This is a log of changes to xdraw, in latest-first order.

Jul 21st, 2001
 * Bugfix: \\ and \" escapes in strings didn't work right.
 * Some code for using an Xaw Viewport widget on top of xdraw, to
   provide for automatic creation of scrollbars when the window is
   resized to be smaller. This means that xdraw requires Xaw (the
   Athena Widgets) now; please tell me if this is a problem for you.
 * Released version 1.40. (This release doesn't have major changes,
   but I used the version number 1.40 instead of 1.16, because xdraw
   is now quite different from what version 1.00 was.)

Nov 28th, 2000
 * xdraw-mzscheme.scm: Bugfix: wait-for-event got stuck in a busy loop
   if the xdraw window was closed while waiting.

Nov 5th, 2000
 * Added shapemask support for pixmap commands. The user still can't
   modify the shapemasks, but at least transparent colors work.
 * New commands: SetClipMask, ClearClipMask, SetClipOrigin.
 * Bugfix: Drawing commands didn't work properly immediately after a
   ResizeWindow command.
 * Minor improvements to manual page.
 * Released version 1.15.

Aug 10th, 2000
 * Added ReportMotionEvents and GetPointerPosition.
 * Added command-line options -ignorebreak, -nofileread and
   -nofilewrite.
 * xdraw doesn't ignore SIGINT by default anymore - use -ignorebreak
   if you want it to.
 * Added an application defaults file (not required for xdraw to
   work).
 * Documented X resources better in the manual page.
 * Released version 1.14.

Jul 23rd, 2000
 * xdraw-mzscheme.scm: Added peek-event.
 * README.Scheme: Documented peek-event.
 * Minor changes to README.
 * Added impressionist-landscape example.
 * Wrote examples/README.

Jul 9th, 2000
 * xdraw-mzscheme.scm: Slight modifications to command buffering code.
 * README.Scheme: Documented command buffering.
 * Updated README and TODO files.

Jul 6th, 2000
 * Documented pixmap commands in the manual page.
 * Switched to use XtAppWarning instead of the older XtWarning.
 * Non-public release of version 1.13 beta.

Jul 5th, 2000
 * Added support for reading/writing pixmaps from/to files
   (ReadFileToPixmap, WriteFileFromPixmap, WriteFileFromWindow).
 * Added GetPixmapSize.

Jul 4th, 2000
 * Bugfix: DrawRectangles used to always draw into the window.
 * Bugfix: CopyArea copied from the window instead of the buffer.
 * Added pixmap support (CreatePixmap, FreePixmap, FreePixmaps,
   SetDrawTargetToPixmap, SetDrawTargetToWindow, CopyAreaFromPixmap).

Jun 17th, 2000
 * Wrote a dictionary data type (DrawCanvas-dict.c), and made the font
   and color caches use it. Currently it is just an unordered growable
   array.

Jun 14th, 2000
 * xdraw-mzscheme.scm: Changed format-xdraw-command to append a
   terminating newline (which is logically part of the command).
   This changed the display-cmd and error reporting output format
   slightly.
 * xdraw-mzscheme.scm: Some optimization and cosmetic changes.
 * xdraw-mzscheme.scm: Added command buffering, with new messages
   'buffer-commands and 'flush-command-buffer.

Jun 13th, 2000
 * xdraw-mzscheme.scm: Added support for sending multiple preformatted
   commands in one 'send or 'send-with-result.
 * Did some speed tests on the Scheme interface, in an attempt to make
   it faster.

Jan 15th, 2000
 * Fixed a stupid bug in ExitOnEvent.
 * Added columnbar example (using AWK).

Jan 14th, 2000
 * Added ExitOnEvent.

Dec 19th, 1999
 * xdraw-mzscheme.scm: Bugfix: Actually searches for the xdraw binary
   in the PATH now...

Dec 8th, 1999
 * Fixed a minor bug: Some event reporting commands didn't check for
   spurious arguments after the required ones.

Nov 12th, 1999
 * xdraw-mzscheme.scm: Bugfix: wait-for-event and send-with-result
   used to go into an infinite loop if the xdraw process was
   prematurely killed (whoops...). Fixed them to stop at an
   end-of-file (and return the empty list in this case).
 * xdraw-mzscheme.scm: Now checks the cleaned-up status before trying
   to read or write to xdraw. Should be slightly safer to use.
   send-with-result and wait-for-event may now return the empty list
   if the process has exited.
 * Changed version number to 1.12. Released version 1.12.

Oct 17th, 1999
 * xdraw-mzscheme.scm: Bugfix: Error detection used to go into an
   infinite loop if the xdraw process was prematurely closed. Fixed it
   to stop at an end-of-file.
 * Changed version number to 1.11. Released version 1.11.

Oct 16th, 1999
 * xdraw-mzscheme.scm: Bugfix: Now 'exit doesn't try to send the Exit
   command if xdraw has already exited.
 * Actually documented the new ResizeWindow command...

Oct 10th, 1999
 * Completed README.Scheme.
 * Fixed a small formatting bug in the manual page.
 * Changed version number to 1.10. Released version 1.10.

Oct 4th, 1999
 * Added the ResizeWindow command, and set *allowShellResize to true
   in the fallback resources.
 * Tried to make manual page more readable for someone who doesn't
   know anything about Xlib.
 * Started updating README.Scheme.

Aug 26th, 1999
 * xdraw-mzscheme.scm: Added wait-for-event message. Fixed small bugs.
 * Minor corrections to manual page.

Aug 25th, 1999
 * xdraw-mzscheme.scm: Added a simpler interface (new-xdraw) on top of
   the old one.
 * Changed version number to 1.09 alpha.

Aug 24th, 1999
 * xdraw-mzscheme.scm: Cleaned up the code some more.
 * xdraw-mzscheme.scm: Added event support.

Aug 23rd, 1999
 * xdraw-mzscheme.scm: Cleaned up code somewhat.
 * xdraw-mzscheme.scm: Now reports errors from previous commands.
   Added wait-for-errors option (separate from error-handling).

Aug 7th, 1999
 * Documented event reporting commands on the manual page.

Jul 31st, 1999
 * Created version.h. 1.08 alpha is the current version.
 * Added the GetxdrawVersion command.
 * Event reporting code should be complete now.

Jul 27th, 1999
 * Changed manual page to use nicer notation.
 * Continued writing event reporting code.

Jul 26th, 1999
 * Added proper event reporting (Report*Events). Not completely
   finished yet...
 * Fixed a stupid bug where unknown commands returned an unspecified
   value.
 * Added "*input: true" to xdraw's fallback resources (which are
   always read because we don't have an application defaults file), so
   now xdraw can get the keyboard focus. I don't know why the resource
   was false by default.
 * Changed the top-level widget to be SessionShell instead of
   ApplicationShell, as X11R6 recommends.

Jul 25th, 1999
 * Added ReportExposeEvents and ReportResizeEvents, so now xdraw
   reports some events.

Jun 21st, 1999
 * Added the GetTextExtents and GetWindowSize commands. Now xdraw
   finally gives some information to the parent process on request.

Jun 20th, 1999
 * Added the SetWindowDrawing and RedrawWindowArea commands.

Jun 19th, 1999
 * xdraw-mzscheme.scm: Switched to a nicer interface (which isn't
   backwards-compatible!).
 * xdraw-mzscheme.scm: Added debugging features: displaying commands and
   input, automatic (but slow) error handling.

Jun 15th, 1999
 * The DrawCanvas widget now copies the old pixmap into the new one when
   it is resized.
 * xdraw-mzscheme.scm: Should be thread-safe now.

Jun 6th, 1999
 * Added the DrawJustifiedString command.

May 26th, 1999
 * xdraw-mzscheme.scm: Should be somewhat faster now.
	
Sep 27th, 1998
 * Initial public release (version 1.00).
