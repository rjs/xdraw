/*
 * DrawCanvas -- A widget that accepts drawing commands
 * Copyright (C) 1998-2001 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

/*
 * DrawCanvas-dict.h -- Header file for dictionary data structure
 */

#include <X11/IntrinsicP.h>

typedef union {
  unsigned long ul;
  Font f;
  struct {
    Pixmap p;
    Pixmap s;
  } pm;
} DictData;

typedef struct {
  int allocated, used;
  char **keys;
  DictData *data;
} Dictionary;

Dictionary *dict_create(void);
void dict_destroy(Dictionary *dict);
DictData *dict_find(Dictionary *dict, const char *key);
void dict_remove(Dictionary *dict, const char *key);
void dict_insert(Dictionary *dict, const char *key, DictData data);
int dict_emptyp(Dictionary *dict);
DictData dict_remove_any(Dictionary *dict);
