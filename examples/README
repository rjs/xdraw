Example programs for xdraw
==========================

This directory contains some example programs for using xdraw in
various programming languages.

Contents:

 * columnbar, columnbar.awk: A simple example of using xdraw in AWK.
   Runs a command and displays its output in an xdraw window along
   with a "bar chart" of a specified column.

 * impressionist-landscape.scm: An example that uses xdraw's MzScheme
   interface to draw a coastal landscape that looks like an
   impressionist style painting.

See the individual files for details, usage instructions and copyright
information.

Please note that some of the more complex examples are copyrighted
under the GNU General Public License (GPL), while others are in the
public domain. You should not copy nontrivial parts of GPLed code,
unless you license your code also under the GPL.
