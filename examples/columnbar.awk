# AWK program for use by the columnbar shell script
#
# Placed in the public domain by the author, Riku Saikkonen.

function maxofarray(a,     v, m) {
  m = 0;
  for (v in a)
    if (a[v] > m)
      m = a[v];
  return m;
}

BEGIN {
# You can customise these
  xdrawpath = "xdraw";
  fontname = "9x15";
  fontcharw = 9;
  fontlineh = 15;
  barwidth = 100;
  barheight = 10;
  sep = 8;
  bgcolor = "black";
  textcolor = "white";
  barcolor = "green";
  
  mycol = COL;
  mycommand = COMMAND;
  ln = 0;
  wwidth = 0;
}

{
  text[ln] = $0;
  val[ln] = (NF >= mycol && ($mycol + 0.0) > 0.0) ? ($mycol + 0.0) : 0;
  ln++;
  
  if (wwidth < length($0) * fontcharw + barwidth + 3 * sep)
    wwidth = length($0) * fontcharw + barwidth + 3 * sep;
}

END {
  if (ln < 1)
    exit 0;
  
  wheight = ln * fontlineh + 2 * sep;
  maxval = maxofarray(val) + 0.0;
  
  commands = \
    "SetForeground \"" bgcolor "\"\n" \
    "FillRectangle 0 0 " (wwidth + 1) " " (wheight + 1) "\n" \
    "SetForeground \"" textcolor "\"\n" \
    "SetFont \"" fontname "\"\n";
  
  curpos = fontlineh + sep;
  for (i = 0; i < ln; i++) {
    if (text[i] != "")
      commands = commands \
	"DrawString " (barwidth + 2 * sep) " " curpos \
	" \"" text[i] "\"\n";
    curpos += fontlineh;
  }

  if (maxval > 0.0) {
    commands = commands \
      "SetForeground \"" barcolor "\"\n";
    
    curpos = fontlineh + sep;
    for (i = 0; i < ln; i++) {
      if (val[i] > 0)
	commands = commands \
	  "FillRectangle " sep " " \
	  (curpos - barheight) " " \
	  int(barwidth * (val[i] / maxval) + 1) " " \
	  (barheight + 1) "\n";
      curpos += fontlineh;
    }
  }
  
  print commands \
    "ReportButtonEvents on off\n" \
    "ExitOnEvent on" \
    | xdrawpath " -geometry " wwidth "x" wheight " -title '" mycommand "'";
  
  exit 0;
}
