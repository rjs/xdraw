;;;
;;; impressionist-landscape.scm - An example that uses xdraw's
;;; MzScheme interface to draw a coastal landscape that looks like an
;;; impressionist style painting
;;;
;;; Copyright (C) 1999-2000 Riku Saikkonen
;;;
;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License along
;;; with this program; if not, write to the Free Software Foundation, Inc.,
;;; 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
;;;
;;; Author: Riku Saikkonen <rjs@netti.fi>
;;;
;;; Usage:
;;;
;;; Load this file and evaluate (draw-painting) to draw the picture.
;;; Evaluate (draw-params) to see most of the parameters used in the
;;; painting in a visual form.
;;;
;;; This code has not been documented very well, but works as a
;;; nontrivial example of using xdraw. Actually, from xdraw's point of
;;; view, this is a quite trivial example, since it only changes
;;; colors and draws filled polygons.
;;;

;;; Helper procedures

;; Sorts the list l using the before? predicate. Uses merge sort.
(define (sort l before?)
  (define (do-sort l len)
    (if (= len 1)
	(list (car l))
	(let ((mid (floor (/ len 2))))
	  (merge (do-sort l mid)
		 (do-sort (list-tail l mid) (- len mid))))))
  (define (merge l1 l2)
    (cond ((null? l1) l2)
	  ((null? l2) l1)
	  ((before? (car l1) (car l2))
	   (cons (car l1) (merge (cdr l1) l2)))
	  (else
	   (cons (car l2) (merge l1 (cdr l2))))))
  (if (null? l)
      '()
      (do-sort l (length l))))

;; Returns a function that always returns val
(define (const-func val)
  (lambda args val))

;; Returns a uniformly distributed random number between mean-maxdev
;; and mean+maxdev
(define (rnd-uniform mean maxdev)
  (define (rnd-1-around-0)
    (- (/ (random 1000000000) 1000000000.0)
       0.5))
  (+ mean (* 2 maxdev (rnd-1-around-0))))

;;; Points
;;; Implemented as lists so they can be passed directly to xdraw.
;;; A "shape" is a list of points that describe a polygon.

(define (make-point x y) (list x y))
(define (point-x point) (car point))
(define (point-y point) (cadr point))

;; Converts a list of points to a format readable by xdraw
(define (points->xdraw plist) plist)

;; Adds two points together, component-wise
(define (point+ p1 p2) (map + p1 p2))

(define (randomize-point point randomness)
  (point+ point
	  (make-point (rnd-uniform 0 randomness)
		      (rnd-uniform 0 randomness))))

(define (randomize-shape points randomness)
  (map (lambda (p)
	 (randomize-point p (/ randomness 5)))
       points))

;;; Colors

(define (make-color r g b) (list r g b))
(define (color-r color) (car color))
(define (color-g color) (cadr color))
(define (color-b color) (caddr color))

;; Converts a color to a format readable by xdraw
(define (color->xdraw color)
  (define (c->string c)			; rounds components at 0.05
    (number->string (min 1.0 (max 0.0 (/ (floor (* c 20)) 20.0)))))
  (string-append "rgbi:"
		 (c->string (color-r color))
		 "/"
		 (c->string (color-g color))
		 "/"
		 (c->string (color-b color))))

;; Adds two colors together, component-wise
(define (color+ c1 c2) (map + c1 c2))

(define (randomize-color color randomness)
  (let ((bri-mod (rnd-uniform 0 (/ randomness 20))))
    (color+ color
	    (make-color bri-mod bri-mod bri-mod))))

;;; Dots and filling areas

;; Fills a shape using a solid color
(define (fill-shape shape color)
  (xo 'send 'SetForeground (color->xdraw color))
  (xo 'send 'FillPolygon (points->xdraw shape)
      'Complex 'Origin))

(define (draw-dot shape color pos randomness)
  (cond ((= randomness 0)
	 (fill-shape (map (lambda (p) (point+ pos p))
			  shape)
		     color))
	(else				; Apply randomness
	 (draw-dot (randomize-shape shape randomness)
		   (randomize-color color randomness)
		   (randomize-point pos randomness)
		   0))))

;; Fills an area, calling (filler-proc x y) on all the dots to be drawn
(define (fill-area-using area dist-vec filler-proc)
  (define (intersect-x y p1 p2)		; ret: x / 'p1 / 'p2 / 'both / #f
    (cond ((= y (point-y p1) (point-y p2))
	   'both)			; horizontal line at y
	  ((= y (point-y p1))		; intersects at p1
	   'p1)
	  ((= y (point-y p2))		; intersects at p2
	   'p2)
	  ((not (or (< (point-y p1) y (point-y p2))
		    (< (point-y p2) y (point-y p1))))
	   #f)				; line doesn't intersect
	  ((= (point-x p1) (point-x p2))
	   (point-x p1))		; vertical line
	  (else				; slanting line
	   (+ (point-x p1) (/ (* (- (point-x p2) (point-x p1))
				 (- y (point-y p1)))
			      (- (point-y p2) (point-y p1)))))))
  (define (intersect-line y p1 p2 p3)	; intersection of p1-p2
    (let ((ix (intersect-x y p1 p2)))
      (cond ((number? ix)
	     (list (list ix 't)))	; toggle filling here
	    ((or (not ix) (eq? ix 'p1))
	     '())			; no intersection
	    ((eq? ix 'both)
	     (list (list (point-x p1) 's) ; start fill here
		   (list (point-x p2) 'e))) ; end fill here
	    (else			; ix = 'p2
	     (let ((ix-skipped (intersect-x y p1 p3)))
	       (if (number? ix-skipped)
		   (list (list (point-x p2) 't)) ; intersects here
		   '()))))))		; no intersection
  (define (all-intersections y points)
    (define (iter left result)
      (if (null? (cddr left))
	  result
	  (iter (cdr left)
		(append (intersect-line y
					(car left)
					(cadr left)
					(caddr left))
			result))))
    (sort (iter (append points
			(list (car points))
			(list (cadr points)))
		'())
	  (lambda (i1 i2)
	    (< (car i1) (car i2)))))
  (define (fill-hline y intersections)
    (define (fill-x-area x dx maxx)
      (cond ((> x maxx)
	     'ok)
	    (else
	     (filler-proc x y)
	     (fill-x-area (+ x dx) dx maxx))))
    (define (iter is-left state)
      (cond ((or (null? is-left) (null? (cdr is-left)))
	     'done)
	    ((eq? (cadar is-left) 's)
	     (fill-x-area (caar is-left)
			  (point-x dist-vec)
			  (caadr is-left))
	     (iter (cdr is-left) #t))
	    ((eq? (cadar is-left) 'e)
	     (iter (cdr is-left) #f))
	    (else			; 't = toggle
	     (if (not state)
		 (fill-x-area (caar is-left)
			      (point-x dist-vec)
			      (caadr is-left)))
	     (iter (cdr is-left) (not state)))))
    (iter intersections #f))
  (define (min-y area)
    (inexact->exact (floor (apply min (map point-y area)))))
  (define (max-y area)
    (inexact->exact (ceiling (apply max (map point-y area)))))
  (define (iter y dy maxy area)
    (cond ((> y maxy)
	   'done)
	  (else
	   (fill-hline y (all-intersections y area))
	   (iter (+ y dy) dy maxy area))))
  (iter (min-y area)
	(point-y dist-vec)
	(max-y area)
	area))

;;; Things

(define (make-thing area dist dot-shape color-func randomness)
  (list area dist dot-shape color-func randomness))

(define (thing-area thing) (car thing))
(define (thing-dist thing) (cadr thing))
(define (thing-dot-shape thing) (caddr thing))
(define (thing-color-func thing) (cadddr thing))
(define (thing-randomness thing) (car (cddddr thing)))

(define (fill-thing-background thing base-color)
  (fill-shape (thing-area thing)
	      (color+ base-color
		      ((thing-color-func thing)
		       (point-x (car (thing-area thing)))
		       (point-y (car (thing-area thing)))))))

(define (draw-thing-dots thing)
  (fill-area-using (thing-area thing)
		   (thing-dist thing)
		   (lambda (x y)
		     (draw-dot (thing-dot-shape thing)
			       ((thing-color-func thing)
				x y)
			       (make-point x y)
			       (thing-randomness thing)))))

;; Returns a color function that computes a brightness gradient
(define (color-grad-func base-color base-at-x base-at-y x-grad y-grad)
  (lambda (x y)
    (let ((bri-mod (+ (* x-grad (- x base-at-x))
		      (* y-grad (- y base-at-y)))))
      (color+ base-color
	      (make-color bri-mod bri-mod bri-mod)))))

(define (draw-sample-dot thing pos)
  (draw-dot (thing-dot-shape thing)
	    ((thing-color-func thing)
	     (point-x (car (thing-area thing)))
	     (point-y (car (thing-area thing))))
	    pos
	    0))

;;; Initialisation

(define xo '())

(define (init-xdraw! width height title)
  (set! xo (new-xdraw "-geometry"
		      (string-append (number->string width)
				     "x"
				     (number->string height))
		      "-title"
		      title)))

;;; Configuration

(define xo-width 501)
(define xo-height 301)

(define base-background (make-color 0.16 0.14 0.1))

(define sky
  (let ((area '((0 0) (500 0) (500 63) (0 63)))
	(dist (make-point 15 6))
	(dot-shape '((-12 0) (-9 2) (-4 3) (0 5) (2 4) (6 3) (9 1) (5 -3)
		     (0 -5) (-4 -4) (-10 -2)))
	(color-func (color-grad-func (make-color 0.02 0.7 0.9)
				     0 45
				     0 1/120))
	(randomness 2))
    (make-thing area dist dot-shape color-func randomness)))

(define sun
  (let ((area '((390 25) (401 14) (409 14) (420 25) (409 36) (401 36)))
	(dist (make-point 1 1))
	(dot-shape '((-2 0) (0 2) (2 0)))
	(color-func (const-func (make-color 1.0 0.97 0.1)))
	(randomness 2))
    (make-thing area dist dot-shape color-func randomness)))

(define water
  (let ((area '((0 60) (500 60) (500 255) (380 225) (130 215) (0 185)))
	(dist (make-point 9 3))
	(dot-shape '((-9 1) (-6 2) (-2 3) (0 4) (2 3) (5 2) (8 1) (10 0)
		     (7 -1) (4 -2) (0 -1) (-2 -2) (-6 -1) (-7 0)))
	(color-func (color-grad-func (make-color 0.05 0.55 0.8)
				     500 160
				     1/7000 -1/700))
	(randomness 3))
    (make-thing area dist dot-shape color-func randomness)))

(define sun-reflection
  (let ((area '((384 74) (425 74) (413 223) (397 223)))
	(dist (make-point 12 16))
	(dot-shape '((-7 1) (-6 2) (0 3) (4 2) (7 1) (7 0) (6 -1) (4 -2)
		     (0 -1) (-2 -2) (-6 -1)))
	(color-func (const-func (make-color 0.8 0.85 0.35)))
	(randomness 5))
    (make-thing area dist dot-shape color-func randomness)))

(define very-small-isle
  (let ((area '((0 58) (100 58) (103 60) (40 61)))
	(dist (make-point 1 3))
	(dot-shape '((-2 0) (-1 -2) (0 -4) (1 -3) (3 1) (0 2)))
	(color-func (const-func (make-color 0 0.33 0.31)))
	(randomness 2))
    (make-thing area dist dot-shape color-func randomness)))

(define small-isle
  (let ((area '((250 60) (253 57) (360 57) (365 60) (290 61)))
	(dist (make-point 1 3))
	(dot-shape '((-2 0) (-1 -2) (0 -5) (1 -3) (3 1) (0 2)))
	(color-func (const-func (make-color 0 0.35 0.26)))
	(randomness 2))
    (make-thing area dist dot-shape color-func randomness)))

(define big-isle-ground
  (let ((area '((92 73) (94 65) (201 63) (208 72) (180 74)))
	(dist (make-point 4 3))
	(dot-shape '((-3 0) (-2 2) (1 2) (4 0) (2 -1) (1 -3) (-1 -2)))
	(color-func (const-func (make-color 0.2 0.55 0.05)))
	(randomness 1))
    (make-thing area dist dot-shape color-func randomness)))

(define big-isle-trees
  (let ((area '((95 72) (101 54) (185 52) (196 55) (203 73)))
	(dist (make-point 2 7))
	(dot-shape '((-4 0) (-1 -6) (0 -10) (1 -7) (4 1) (0 3)))
	(color-func (const-func (make-color 0 0.41 0.04)))
	(randomness 2))
    (make-thing area dist dot-shape color-func randomness)))

(define grass
  (let ((area '((0 310) (500 310) (500 250) (380 220) (130 210) (0 180)))
	(dist (make-point 4 6))
	(dot-shape '((-2 0) (-1 3) (1 2) (3 -1) (2 -4) (1 -8) (0 -6)
		     (-1 -3)))
	(color-func (color-grad-func (make-color 0.15 0.65 0.04)
				     500 200
				     1/9000 -1/800))
	(randomness 4))
    (make-thing area dist dot-shape color-func randomness)))

(define flowers
  (let ((area '((0 230) (300 250) (310 260) (0 280)))
	(dist (make-point 15 11))
	(dot-shape '((-4 0) (-2 2) (-1 3) (1 4) (2 2) (4 0) (3 -1) (1 -4)
		     (-1 -2) (-3 -1)))
	(color-func (const-func (make-color 1.05 0.1 0.05)))
	(randomness 4))
    (make-thing area dist dot-shape color-func randomness)))

(define (draw-painting)
  (init-xdraw! xo-width xo-height "Painting")
  (fill-thing-background sky base-background)
  (fill-thing-background water base-background)
  (fill-thing-background grass base-background)
  (draw-thing-dots sky)
  (draw-thing-dots sun)
  (draw-thing-dots water)
  (draw-thing-dots sun-reflection)
  (draw-thing-dots very-small-isle)
  (draw-thing-dots small-isle)
  (draw-thing-dots big-isle-ground)
  (draw-thing-dots big-isle-trees)
  (draw-thing-dots grass)
  (draw-thing-dots flowers)
  'done)

(define (draw-params)
  (init-xdraw! xo-width (+ xo-height 30) "Parameters")
  (fill-thing-background sky base-background)
  (fill-thing-background water base-background)
  (fill-thing-background grass base-background)
  (fill-thing-background sun base-background)
  (fill-thing-background sun-reflection base-background)
  (fill-thing-background very-small-isle base-background)
  (fill-thing-background small-isle base-background)
  (fill-thing-background big-isle-ground base-background)
  (fill-thing-background big-isle-trees base-background)
  (fill-thing-background flowers base-background)
  (let ((sample-y (+ xo-height 15)))
    (xo 'send 'SetForeground "grey")
    (xo 'send 'FillRectangle 0 xo-height xo-width 30)
    (draw-sample-dot sky (make-point 15 sample-y))
    (draw-sample-dot water (make-point 30 sample-y))
    (draw-sample-dot grass (make-point 45 sample-y))
    (draw-sample-dot sun (make-point 60 sample-y))
    (draw-sample-dot sun-reflection (make-point 75 sample-y))
    (draw-sample-dot very-small-isle (make-point 90 sample-y))
    (draw-sample-dot small-isle (make-point 90 sample-y))
    (draw-sample-dot big-isle-ground (make-point 105 sample-y))
    (draw-sample-dot big-isle-trees (make-point 120 sample-y))
    (draw-sample-dot flowers (make-point 135 sample-y)))
  'done)
