/*
 * DrawCanvas -- A widget that accepts drawing commands
 * Copyright (C) 1998-2001 Riku Saikkonen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Author: Riku Saikkonen <rjs@netti.fi>
 */

/*
 * DrawCanvas-commands.c -- Functions for the commands
 *
 * This file should be the only thing that needs to be changed to add
 * new (simple) commands.
 *
 * Note: This file is _included_ (with #include) in DrawCanvas.c!
 * (See the comment in DrawCanvas.c for why.)
 */

/*
 * Helper functions for the command handlers
 */

/* 
 * Allocate and fill an array of XPoints by parsing them from a list.
 * Sets *xpp to a pointer to the newly-allocated list and *sizep to
 * the number of elements in it. Returns 1 if ok, or 0 if failed.
 * 
 * The list is allocated using parse_malloc(), so it will be freed
 * automatically by end_parse().
 */
static int read_xpoint_list(DrawCanvasWidget ww, XPoint **xppp,
  int *sizep)
{
  int point_list_size, i, tmpi;
  XPoint *point_list;

  if (!parse_list_start(ww))
    return 0;

  if (!parse_list_count_elements(ww, &point_list_size))
    return 0;

  point_list = (XPoint *)
    parse_malloc(ww, point_list_size * sizeof(XPoint));

  i = 0;
  while (i < point_list_size)
  {
    if (!parse_int(ww, &tmpi))
      return 0;
    point_list[i].x = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    point_list[i].y = tmpi;
    
    i++;
    if (i == point_list_size)
      break;
    else if (!parse_list_next_element(ww))
      return 0;
  }
  
  if (!parse_list_end(ww))
    return 0;

  /* Successful! */
  *xppp = point_list;
  *sizep = point_list_size;

  return 1;
}

/* 
 * Same as read_xpoint_list, but this is for XRectangles.
 * (don't you love statically typed languages?)
 */
static int read_xrectangle_list(DrawCanvasWidget ww, XRectangle **xrpp,
  int *sizep)
{
  int rect_list_size, i, tmpi;
  XRectangle *rect_list;

  if (!parse_list_start(ww))
    return 0;

  if (!parse_list_count_elements(ww, &rect_list_size))
    return 0;

  rect_list = (XRectangle *)
    parse_malloc(ww, rect_list_size * sizeof(XRectangle));

  i = 0;
  while (i < rect_list_size)
  {
    if (!parse_int(ww, &tmpi))
      return 0;
    rect_list[i].x = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    rect_list[i].y = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    rect_list[i].width = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    rect_list[i].height = tmpi;
    
    i++;
    if (i == rect_list_size)
      break;
    else if (!parse_list_next_element(ww))
      return 0;
  }
  
  if (!parse_list_end(ww))
    return 0;

  /* Successful! */
  *xrpp = rect_list;
  *sizep = rect_list_size;

  return 1;
}

/* 
 * Same as read_xpoint_list, but this is for XArcs.
 */
static int read_xarc_list(DrawCanvasWidget ww, XArc **xapp, int *sizep)
{
  int arc_list_size, i, tmpi;
  XArc *arc_list;

  if (!parse_list_start(ww))
    return 0;

  if (!parse_list_count_elements(ww, &arc_list_size))
    return 0;

  arc_list = (XArc *)
    parse_malloc(ww, arc_list_size * sizeof(XArc));

  i = 0;
  while (i < arc_list_size)
  {
    if (!parse_int(ww, &tmpi))
      return 0;
    arc_list[i].x = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    arc_list[i].y = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    arc_list[i].width = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    arc_list[i].height = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    arc_list[i].angle1 = tmpi;

    if (!parse_int(ww, &tmpi))
      return 0;
    arc_list[i].angle2 = tmpi;
    
    i++;
    if (i == arc_list_size)
      break;
    else if (!parse_list_next_element(ww))
      return 0;
  }
  
  if (!parse_list_end(ww))
    return 0;

  /* Successful! */
  *xapp = arc_list;
  *sizep = arc_list_size;

  return 1;
}

/*
 * Read a symbol, looking it up in symbol_table. Returns the index to
 * the found symbol in the table, or -1 on error.
 * table_size = number of elements in symbol_table
 */
static int read_symbol_in_table(DrawCanvasWidget ww,
  char **symbol_table, int table_size)
{
  char *sym;
  int index;

  if (!parse_symbol(ww, &sym))
    return -1;

  index = find_case_string(sym, symbol_table, table_size);

  if (index < 0)
  {
    /* The mark is at the next token now... */
    parse_error(ww, 1, "Unrecognised symbol (before the mark).");
    return -1;
  }

  return index;
}

/*
 * Read a boolean (on/off) symbol. Returns 1 if on, 0 if off, or -1 on
 * error.
 */
static int read_boolean_symbol(DrawCanvasWidget ww)
{
  static char *onoff_names[] = { "off", "on" };
  static int onoff_values[] = { 0, 1 };
  static int num_onoffs = 2;

  int onoffi;

  if ((onoffi = read_symbol_in_table(ww, onoff_names, num_onoffs)) < 0)
    return onoffi; /* error return */

  return onoff_values[onoffi];
}

/* Get the size from a pixmap */
static void get_pixmap_size(DrawCanvasWidget ww, Pixmap p,
  unsigned int *width_return, unsigned int *height_return)
{
  Window root;
  int x, y;
  unsigned int border_width, depth;

  XGetGeometry(XtDisplay(ww), p, &root, &x, &y,
    width_return, height_return, &border_width, &depth);
}

/*
 * The command handlers
 * These all return 1 if ok, or 0 on parsing failure.
 * We draw into the pixmap pm and possibly the window.
 */

/* Special commands */

static int cmd_exit(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  ww->draw.should_exit = 1;

  return 1;
}

static int cmd_reset(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  /* Reset our state */
  ww->draw.drawinwindow_set = 1;
  ww->draw.processexpose = 1;

  /* Free and reallocate the GCs and the GC stack */
  deinit_gcs(ww);
  init_gcs(ww);

  /* Clear the buffer pixmap and window */
  clear_winpm(ww);
  XClearWindow(XtDisplay(ww), XtWindow(ww));

  /* Reset DrawTarget */
  ww->draw.pm = ww->draw.winpm;
  ww->draw.drawinwindow = 1;

  /* Free fonts, colors and pixmaps */
  free_fonts(ww);
  free_colors(ww);
  free_pixmaps(ww);
  
  return 1;
}

static int cmd_resetgc(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  free_dgc(ww);
  new_dgc(ww);

  return 1;
}

static int cmd_pushgc(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  gcstack_push_and_reset(ww);

  return 1;
}

static int cmd_popgc(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  gcstack_pop(ww);

  return 1;
}

static int cmd_setwindowdrawing(DrawCanvasWidget ww)
{
  int commands, expose;

  if ((commands = read_boolean_symbol(ww)) < 0)
    return 0;

  if ((expose = read_boolean_symbol(ww)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  ww->draw.drawinwindow_set = commands;
  if (ww->draw.pm == ww->draw.winpm)
    ww->draw.drawinwindow = ww->draw.drawinwindow_set;

  ww->draw.processexpose = expose;

  return 1;
}

static int cmd_redrawwindowarea(DrawCanvasWidget ww)
{
  int x, y, width, height;

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_int(ww, &width))
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XCopyArea(XtDisplay(ww), ww->draw.winpm, XtWindow(ww), ww->draw.pmgc,
    x, y, width, height, x, y);

  return 1;
}

static int cmd_resizewindow(DrawCanvasWidget ww)
{
  int width, height;
  Arg args[2];

  if (!parse_int(ww, &width))
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XtSetArg(args[0], XtNwidth, width);
  XtSetArg(args[1], XtNheight, height);
  XtSetValues((Widget) ww, args, 2);

  /* Resize the internal pixmap immediately to make drawing commands
   * work before the Resize method gets called. */
  resize_winpm(ww, width, height);

  return 1;
}

/* GC manipulation commands */

static int cmd_setforeground(DrawCanvasWidget ww)
{
  char *name;
  unsigned long color;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  color = get_color(ww, name);
  XSetForeground(XtDisplay(ww), ww->draw.dgc, color);

  return 1;
}

static int cmd_setbackground(DrawCanvasWidget ww)
{
  char *name;
  unsigned long color;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  color = get_color(ww, name);
  XSetBackground(XtDisplay(ww), ww->draw.dgc, color);

  return 1;
}

static int cmd_setfunction(DrawCanvasWidget ww)
{
  static char *funct_names[] = {
    "clear", "and", "andReverse", "copy", "andInverted", "noop",
    "xor", "or", "nor", "equiv", "invert", "orReverse",
    "copyInverted", "orInverted", "nand", "set"
  };
  static int funct_values[] = {
    GXclear, GXand, GXandReverse, GXcopy, GXandInverted, GXnoop,
    GXxor, GXor, GXnor, GXequiv, GXinvert, GXorReverse,
    GXcopyInverted, GXorInverted, GXnand, GXset
  };
  static int num_functs = 16;

  int functi;

  if ((functi = read_symbol_in_table(ww, funct_names, num_functs)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XSetFunction(XtDisplay(ww), ww->draw.dgc, funct_values[functi]);

  return 1;
}

static int cmd_setlineattributes(DrawCanvasWidget ww)
{
  static char *ls_names[] = { "Solid", "OnOffDash", "DoubleDash" };
  static int ls_values[] = { LineSolid, LineOnOffDash, LineDoubleDash };
  static char *cs_names[] = { "NotLast", "Butt", "Round", "Projecting" };
  static int cs_values[] = {
    CapNotLast, CapButt, CapRound, CapProjecting
  };
  static char *js_names[] = { "Miter", "Round", "Bevel" };
  static int js_values[] = { JoinMiter, JoinRound, JoinBevel };
  static int num_ls = 3, num_cs = 4, num_js = 3;

  int width, lsi, csi, jsi;

  if (!parse_int(ww, &width))
    return 0;

  if ((lsi = read_symbol_in_table(ww, ls_names, num_ls)) < 0)
    return 0;

  if ((csi = read_symbol_in_table(ww, cs_names, num_cs)) < 0)
    return 0;

  if ((jsi = read_symbol_in_table(ww, js_names, num_js)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XSetLineAttributes(XtDisplay(ww), ww->draw.dgc,
    width, ls_values[lsi], cs_values[csi], js_values[jsi]);

  return 1;
}

static int cmd_setdashes(DrawCanvasWidget ww)
{
  int dash_offset, dash_list_size, i, this_dash;
  char *dash_list;

  if (!parse_int(ww, &dash_offset))
    return 0;
  
  if (!parse_list_start(ww))
    return 0;
  if (!parse_list_count_elements(ww, &dash_list_size))
    return 0;

  dash_list = (char *) parse_malloc(ww, dash_list_size * sizeof(char));

  i = 0;
  while (i < dash_list_size)
  {
    if (!parse_int(ww, &this_dash))
      return 0;
    dash_list[i] = (char) this_dash;
    
    i++;
    if (i == dash_list_size)
      break;
    else if (!parse_list_next_element(ww))
      return 0;
  }
  
  if (!parse_list_end(ww))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XSetDashes(XtDisplay(ww), ww->draw.dgc,
    dash_offset, dash_list, dash_list_size);

  return 1;
}

static int cmd_setfillrule(DrawCanvasWidget ww)
{
  static char *rule_names[] = { "EvenOdd", "Winding" };
  static int rule_values[] = { EvenOddRule, WindingRule };
  static int num_rules = 2;

  int rulei;

  if ((rulei = read_symbol_in_table(ww, rule_names, num_rules)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XSetFillRule(XtDisplay(ww), ww->draw.dgc, rule_values[rulei]);

  return 1;
}

static int cmd_setfont(DrawCanvasWidget ww)
{
  char *name;
  Font font;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  font = get_font(ww, name);
  XSetFont(XtDisplay(ww), ww->draw.dgc, font);

  return 1;
}

static int cmd_setcliprectangles(DrawCanvasWidget ww)
{
  static char *ord_names[] = {
    "Unsorted", "YSorted", "YXSorted", "YXBanded"
  };
  static int ord_values[] = { Unsorted, YSorted, YXSorted, YXBanded };    
  static int num_ords = 4;

  int clip_x_origin, clip_y_origin, ordi, rect_list_size;
  XRectangle *rect_list;

  if (!parse_int(ww, &clip_x_origin))
    return 0;

  if (!parse_int(ww, &clip_y_origin))
    return 0;

  if (!read_xrectangle_list(ww, &rect_list, &rect_list_size))
    return 0;

  if ((ordi = read_symbol_in_table(ww, ord_names, num_ords)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XSetClipRectangles(XtDisplay(ww), ww->draw.dgc, clip_x_origin,
    clip_y_origin, rect_list, rect_list_size, ord_values[ordi]);

  return 1;
}

static int cmd_setarcmode(DrawCanvasWidget ww)
{
  static char *mode_names[] = { "Chord", "PieSlice" };
  static int mode_values[] = { ArcChord, ArcPieSlice };
  static int num_modes = 2;

  int modei;

  if ((modei = read_symbol_in_table(ww, mode_names, num_modes)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XSetArcMode(XtDisplay(ww), ww->draw.dgc, mode_values[modei]);

  return 1;
}

static int cmd_setcliporigin(DrawCanvasWidget ww)
{
  int clip_x_origin, clip_y_origin;

  if (!parse_int(ww, &clip_x_origin))
    return 0;

  if (!parse_int(ww, &clip_y_origin))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XSetClipOrigin(XtDisplay(ww), ww->draw.dgc,
    clip_x_origin, clip_y_origin);

  return 1;
}

static int cmd_clearclipmask(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  XSetClipMask(XtDisplay(ww), ww->draw.dgc, None);

  return 1;
}

/* Drawing commands */

static int cmd_copyarea(DrawCanvasWidget ww)
{
  int src_x, src_y, width, height, dest_x, dest_y;
  Display *disp = XtDisplay(ww);
  Window win = XtWindow(ww);

  if (!parse_int(ww, &src_x))
    return 0;

  if (!parse_int(ww, &src_y))
    return 0;

  if (!parse_int(ww, &width))
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_int(ww, &dest_x))
    return 0;

  if (!parse_int(ww, &dest_y))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XCopyArea(disp, ww->draw.pm, ww->draw.pm, ww->draw.dgc,
    src_x, src_y, width, height, dest_x, dest_y);

  if (ww->draw.drawinwindow)
    XCopyArea(disp, ww->draw.pm, win, ww->draw.dgc,
      src_x, src_y, width, height, dest_x, dest_y);

  return 1;
}

static int cmd_drawpoint(DrawCanvasWidget ww)
{
  int x, y;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawPoint(disp, ww->draw.pm, ww->draw.dgc, x, y);

  if (ww->draw.drawinwindow)
    XDrawPoint(disp, XtWindow(ww), ww->draw.dgc, x, y);

  return 1;
}

static int cmd_drawpoints(DrawCanvasWidget ww)
{
  static char *mode_names[] = { "Origin", "Previous" };
  static int mode_values[] = { CoordModeOrigin, CoordModePrevious };    
  static int num_modes = 2;

  int modei, point_list_size;
  XPoint *point_list;
  Display *disp = XtDisplay(ww);

  if (!read_xpoint_list(ww, &point_list, &point_list_size))
    return 0;

  if ((modei = read_symbol_in_table(ww, mode_names, num_modes)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawPoints(disp, ww->draw.pm, ww->draw.dgc,
    point_list, point_list_size, mode_values[modei]);

  if (ww->draw.drawinwindow)
    XDrawPoints(disp, XtWindow(ww), ww->draw.dgc,
      point_list, point_list_size, mode_values[modei]);

  return 1;
}

static int cmd_drawline(DrawCanvasWidget ww)
{
  int x1, y1, x2, y2;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x1))
    return 0;

  if (!parse_int(ww, &y1))
    return 0;

  if (!parse_int(ww, &x2))
    return 0;

  if (!parse_int(ww, &y2))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawLine(disp, ww->draw.pm, ww->draw.dgc, x1, y1, x2, y2);

  if (ww->draw.drawinwindow)
    XDrawLine(disp, XtWindow(ww), ww->draw.dgc, x1, y1, x2, y2);

  return 1;
}

static int cmd_drawlines(DrawCanvasWidget ww)
{
  static char *mode_names[] = { "Origin", "Previous" };
  static int mode_values[] = { CoordModeOrigin, CoordModePrevious };    
  static int num_modes = 2;

  int modei, point_list_size;
  XPoint *point_list;
  Display *disp = XtDisplay(ww);

  if (!read_xpoint_list(ww, &point_list, &point_list_size))
    return 0;

  if ((modei = read_symbol_in_table(ww, mode_names, num_modes)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawLines(disp, ww->draw.pm, ww->draw.dgc,
    point_list, point_list_size, mode_values[modei]);

  if (ww->draw.drawinwindow)
    XDrawLines(disp, XtWindow(ww), ww->draw.dgc,
      point_list, point_list_size, mode_values[modei]);

  return 1;
}

static int cmd_drawsegments(DrawCanvasWidget ww)
{
  int seg_list_size, i, tmpi;
  XSegment *seg_list;
  Display *disp = XtDisplay(ww);

  if (!parse_list_start(ww)) return 0;
  if (!parse_list_count_elements(ww, &seg_list_size)) return 0;

  seg_list = (XSegment *)
    parse_malloc(ww, seg_list_size * sizeof(XSegment));

  i = 0;
  while (i < seg_list_size)
  {
    if (!parse_int(ww, &tmpi)) return 0;
    seg_list[i].x1 = tmpi;
    if (!parse_int(ww, &tmpi)) return 0;
    seg_list[i].y1 = tmpi;
    if (!parse_int(ww, &tmpi)) return 0;
    seg_list[i].x2 = tmpi;
    if (!parse_int(ww, &tmpi)) return 0;
    seg_list[i].y2 = tmpi;
    
    i++;
    if (i == seg_list_size)
      break;
    else if (!parse_list_next_element(ww)) return 0;
  }
  
  if (!parse_list_end(ww)) return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawSegments(disp, ww->draw.pm, ww->draw.dgc,
    seg_list, seg_list_size);

  if (ww->draw.drawinwindow)
    XDrawSegments(disp, XtWindow(ww), ww->draw.dgc,
      seg_list, seg_list_size);

  return 1;
}

static int cmd_drawrectangle(DrawCanvasWidget ww)
{
  int x, y, width, height;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_int(ww, &width))
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawRectangle(disp, ww->draw.pm, ww->draw.dgc, x, y, width, height);

  if (ww->draw.drawinwindow)
    XDrawRectangle(disp, XtWindow(ww), ww->draw.dgc, x, y, width, height);

  return 1;
}

static int cmd_drawrectangles(DrawCanvasWidget ww)
{
  int rect_list_size;
  XRectangle *rect_list;
  Display *disp = XtDisplay(ww);

  if (!read_xrectangle_list(ww, &rect_list, &rect_list_size))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawRectangles(disp, ww->draw.pm, ww->draw.dgc,
    rect_list, rect_list_size);

  if (ww->draw.drawinwindow)
    XDrawRectangles(disp, XtWindow(ww), ww->draw.dgc,
      rect_list, rect_list_size);

  return 1;
}

static int cmd_drawarc(DrawCanvasWidget ww)
{
  int x, y, width, height, angle1, angle2;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_int(ww, &width))
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_int(ww, &angle1))
    return 0;

  if (!parse_int(ww, &angle2))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawArc(disp, ww->draw.pm, ww->draw.dgc,
    x, y, width, height, angle1, angle2);

  if (ww->draw.drawinwindow)
    XDrawArc(disp, XtWindow(ww), ww->draw.dgc,
      x, y, width, height, angle1, angle2);

  return 1;
}

static int cmd_drawarcs(DrawCanvasWidget ww)
{
  int arc_list_size;
  XArc *arc_list;
  Display *disp = XtDisplay(ww);

  if (!read_xarc_list(ww, &arc_list, &arc_list_size))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawArcs(disp, ww->draw.pm, ww->draw.dgc,
    arc_list, arc_list_size);

  if (ww->draw.drawinwindow)
    XDrawArcs(disp, XtWindow(ww), ww->draw.dgc,
      arc_list, arc_list_size);

  return 1;
}

static int cmd_fillrectangle(DrawCanvasWidget ww)
{
  int x, y, width, height;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_int(ww, &width))
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XFillRectangle(disp, ww->draw.pm, ww->draw.dgc, x, y, width, height);

  if (ww->draw.drawinwindow)
    XFillRectangle(disp, XtWindow(ww), ww->draw.dgc, x, y, width, height);
  
  return 1;
}

static int cmd_fillrectangles(DrawCanvasWidget ww)
{
  int rect_list_size;
  XRectangle *rect_list;
  Display *disp = XtDisplay(ww);

  if (!read_xrectangle_list(ww, &rect_list, &rect_list_size))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XFillRectangles(disp, ww->draw.pm, ww->draw.dgc,
    rect_list, rect_list_size);

  if (ww->draw.drawinwindow)
    XFillRectangles(disp, XtWindow(ww), ww->draw.dgc,
      rect_list, rect_list_size);

  return 1;
}

static int cmd_fillpolygon(DrawCanvasWidget ww)
{
  static char *shape_names[] = { "Complex", "Convex", "Nonconvex" };
  static int shape_values[] = { Complex, Convex, Nonconvex };    
  static char *mode_names[] = { "Origin", "Previous" };
  static int mode_values[] = { CoordModeOrigin, CoordModePrevious };    
  static int num_shapes = 3, num_modes = 2;

  int shapei, modei, point_list_size;
  XPoint *point_list;
  Display *disp = XtDisplay(ww);

  if (!read_xpoint_list(ww, &point_list, &point_list_size))
    return 0;

  if ((shapei = read_symbol_in_table(ww, shape_names, num_shapes)) < 0)
    return 0;

  if ((modei = read_symbol_in_table(ww, mode_names, num_modes)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XFillPolygon(disp, ww->draw.pm, ww->draw.dgc, point_list,
    point_list_size, shape_values[shapei], mode_values[modei]);

  if (ww->draw.drawinwindow)
    XFillPolygon(disp, XtWindow(ww), ww->draw.dgc, point_list,
      point_list_size, shape_values[shapei], mode_values[modei]);

  return 1;
}

static int cmd_fillarc(DrawCanvasWidget ww)
{
  int x, y, width, height, angle1, angle2;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_int(ww, &width)) 
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_int(ww, &angle1))
    return 0;

  if (!parse_int(ww, &angle2))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XFillArc(disp, ww->draw.pm, ww->draw.dgc,
    x, y, width, height, angle1, angle2);

  if (ww->draw.drawinwindow)
    XFillArc(disp, XtWindow(ww), ww->draw.dgc,
      x, y, width, height, angle1, angle2);

  return 1;
}

static int cmd_fillarcs(DrawCanvasWidget ww)
{
  int arc_list_size;
  XArc *arc_list;
  Display *disp = XtDisplay(ww);

  if (!read_xarc_list(ww, &arc_list, &arc_list_size))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XFillArcs(disp, ww->draw.pm, ww->draw.dgc,
    arc_list, arc_list_size);

  if (ww->draw.drawinwindow)
    XFillArcs(disp, XtWindow(ww), ww->draw.dgc,
      arc_list, arc_list_size);

  return 1;
}

static int cmd_drawstring(DrawCanvasWidget ww)
{
  int x, y;
  char *string;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_string(ww, &string))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawString(disp, ww->draw.pm, ww->draw.dgc,
    x, y, string, strlen(string));

  if (ww->draw.drawinwindow)
    XDrawString(disp, XtWindow(ww), ww->draw.dgc,
      x, y, string, strlen(string));

  return 1;
}

static int cmd_drawimagestring(DrawCanvasWidget ww)
{
  int x, y;
  char *string;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_string(ww, &string))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XDrawImageString(disp, ww->draw.pm, ww->draw.dgc,
    x, y, string, strlen(string));

  if (ww->draw.drawinwindow)
    XDrawImageString(disp, XtWindow(ww), ww->draw.dgc,
      x, y, string, strlen(string));

  return 1;
}

/* Special drawing commands */

static int cmd_drawroundedrectangle(DrawCanvasWidget ww)
{
  int x, y, w, h, ew, eh;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_int(ww, &w))
    return 0;

  if (!parse_int(ww, &h))
    return 0;

  if (!parse_int(ww, &ew))
    return 0;

  if (!parse_int(ww, &eh))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XmuDrawRoundedRectangle(disp, ww->draw.pm, ww->draw.dgc,
    x, y, w, h, ew, eh);

  if (ww->draw.drawinwindow)
    XmuDrawRoundedRectangle(disp, XtWindow(ww), ww->draw.dgc,
      x, y, w, h, ew, eh);

  return 1;
}

static int cmd_fillroundedrectangle(DrawCanvasWidget ww)
{
  int x, y, w, h, ew, eh;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if (!parse_int(ww, &w))
    return 0;

  if (!parse_int(ww, &h))
    return 0;

  if (!parse_int(ww, &ew))
    return 0;

  if (!parse_int(ww, &eh))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  XmuFillRoundedRectangle(disp, ww->draw.pm, ww->draw.dgc,
    x, y, w, h, ew, eh);

  if (ww->draw.drawinwindow)
    XmuFillRoundedRectangle(disp, XtWindow(ww), ww->draw.dgc,
      x, y, w, h, ew, eh);

  return 1;
}

static int cmd_drawjustifiedstring(DrawCanvasWidget ww)
{
  static char *hjust_names[] = { "Left", "Center", "Right" };
  static int hjust_values[] = { 0, 1, 2 };
  static char *vjust_names[] = { "Baseline", "Top", "Center", "Bottom" };
  static int vjust_values[] = { 0, 1, 2, 3 };
  static char *type_names[] = { "Normal", "Image" };
  static int type_values[] = { 0, 1 };
  static int num_hjusts = 3, num_vjusts = 4, num_types = 2;

  int x, y, hjusti, vjusti, typei;
  char *string;
  int len, direction, font_ascent, font_descent, realx, realy;
  XCharStruct xcs;
  Display *disp = XtDisplay(ww);

  if (!parse_int(ww, &x))
    return 0;

  if (!parse_int(ww, &y))
    return 0;

  if ((hjusti = read_symbol_in_table(ww, hjust_names, num_hjusts)) < 0)
    return 0;

  if ((vjusti = read_symbol_in_table(ww, vjust_names, num_vjusts)) < 0)
    return 0;

  if ((typei = read_symbol_in_table(ww, type_names, num_types)) < 0)
    return 0;

  if (!parse_string(ww, &string))
    return 0;

  len = strlen(string);

  if (!parse_command_end(ww))
    return 0;

  XQueryTextExtents(disp, XGContextFromGC(ww->draw.dgc), string, len,
    &direction, &font_ascent, &font_descent, &xcs);

  /* (XXX) Calculations assume left-to-right fonts */
  switch (hjust_values[hjusti])
  {
    case 0: /* Left */
      realx = x;
      break;
    case 1: /* Center */
      realx = x - xcs.width / 2;
      break;
    case 2: /* Right */
      realx = x - xcs.width + 1;
      break;
    default: /* shouldn't happen */
      return 0;
  }

  /* XXX Calculations assume single-line strings */
  switch (vjust_values[vjusti])
  {
    case 0: /* Baseline */
      realy = y;
      break;
    case 1: /* Top */
      realy = y + font_ascent;
      break;
    case 2: /* Center */
      realy = y + font_ascent - (font_ascent + font_descent) / 2;
      break;
    case 3: /* Bottom */
      realy = y - font_descent + 1;
      break;
    default: /* shouldn't happen */
      return 0;
  }

  if (type_values[typei] == 1) /* Image */
  {
    XDrawImageString(disp, ww->draw.pm, ww->draw.dgc,
      realx, realy, string, len);

    if (ww->draw.drawinwindow)
      XDrawImageString(disp, XtWindow(ww), ww->draw.dgc,
	realx, realy, string, len);
  } else /* Normal */
  {
    XDrawString(disp, ww->draw.pm, ww->draw.dgc,
      realx, realy, string, len);

    if (ww->draw.drawinwindow)
      XDrawString(disp, XtWindow(ww), ww->draw.dgc,
	realx, realy, string, len);
  }

  return 1;
}

/* Input commands */

static int cmd_getwindowsize(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  returnf(ww, "%d %d", ww->core.width, ww->core.height);

  return 1;
}

static int cmd_getxdrawversion(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  returnf(ww, "xdraw \"" XDRAW_VERSION "\"");

  return 1;
}

static int cmd_gettextextents(DrawCanvasWidget ww)
{
  char *string;
  int len, direction, font_ascent, font_descent;
  XCharStruct xcs;

  if (!parse_string(ww, &string))
    return 0;

  len = strlen(string);

  if (!parse_command_end(ww))
    return 0;

  XQueryTextExtents(XtDisplay(ww), XGContextFromGC(ww->draw.dgc),
    string, len, &direction, &font_ascent, &font_descent, &xcs);

  returnf(ww, "%s %d %d %d %d %d %d %d",
    (direction == FontRightToLeft) ? "RightToLeft" : "LeftToRight",
    font_ascent, font_descent,
    xcs.lbearing, xcs.rbearing, xcs.width, xcs.ascent, xcs.descent);

  return 1;
}

static int cmd_getpointerposition(DrawCanvasWidget ww)
{
  Window w = XtWindow(ww);
  Window root, child;
  int root_x, root_y, win_x, win_y;
  unsigned int mask;

  if (!parse_command_end(ww))
    return 0;

  if (XQueryPointer(XtDisplay(ww), w, &root, &child,
    &root_x, &root_y, &win_x, &win_y, &mask) == True)
    returnf(ww, "True %d %d %s", win_x, win_y, statestr(mask));
  else returnf(ww, "False -1 -1 ( )");

  return 1;
}

/* Event handling commands */

static int cmd_reportexposeevents(DrawCanvasWidget ww)
{
  int report;

  if ((report = read_boolean_symbol(ww)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  ww->draw.reportexpose = report;

  return 1;
}

static int cmd_reportresizeevents(DrawCanvasWidget ww)
{
  int report;

  if ((report = read_boolean_symbol(ww)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  ww->draw.reportresize = report;

  return 1;
}

static int cmd_reportkeyevents(DrawCanvasWidget ww)
{
  int press, release;

  if ((press = read_boolean_symbol(ww)) < 0)
    return 0;

  if ((release = read_boolean_symbol(ww)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  change_event_mask(ww,
    (press ? KeyPressMask : 0) | (release ? KeyReleaseMask : 0),
    (press ? 0 : KeyPressMask) | (release ? 0 : KeyReleaseMask));

  return 1;
}

static int cmd_reportbuttonevents(DrawCanvasWidget ww)
{
  int press, release;

  if ((press = read_boolean_symbol(ww)) < 0)
    return 0;

  if ((release = read_boolean_symbol(ww)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  change_event_mask(ww,
    (press ? ButtonPressMask : 0) | (release ? ButtonReleaseMask : 0),
    (press ? 0 : ButtonPressMask) | (release ? 0 : ButtonReleaseMask));

  return 1;
}

static int cmd_reportenterleaveevents(DrawCanvasWidget ww)
{
  int enter, leave;

  if ((enter = read_boolean_symbol(ww)) < 0)
    return 0;

  if ((leave = read_boolean_symbol(ww)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  change_event_mask(ww,
    (enter ? EnterWindowMask : 0) | (leave ? LeaveWindowMask : 0),
    (enter ? 0 : EnterWindowMask) | (leave ? 0 : LeaveWindowMask));

  return 1;
}

static int cmd_reportfocuschangeevents(DrawCanvasWidget ww)
{
  int focus;

  if ((focus = read_boolean_symbol(ww)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  change_event_mask(ww,
    focus ? FocusChangeMask : 0,
    focus ? 0 : FocusChangeMask);

  return 1;
}

static int cmd_reportmotionevents(DrawCanvasWidget ww)
{
  static char *motion_names[] = { "off", "on", "hint" };
  static int num_motions = 3;

  int motioni;

  if ((motioni = read_symbol_in_table(ww, motion_names, num_motions)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;
  
  change_event_mask(ww,
    ((motioni > 0) ? PointerMotionMask : 0) |
    ((motioni == 2) ? PointerMotionHintMask : 0),
    ((motioni < 2) ? PointerMotionHintMask : 0) |
    ((motioni == 0) ? PointerMotionMask : 0));

  return 1;
}

static int cmd_exitonevent(DrawCanvasWidget ww)
{
  int newstate;

  if ((newstate = read_boolean_symbol(ww)) < 0)
    return 0;

  if (!parse_command_end(ww))
    return 0;

  ww->draw.exit_on_event = newstate;

  return 1;
}

/*
 * Pixmap-related commands
 */

static int cmd_createpixmap(DrawCanvasWidget ww)
{
  char *name;
  int width, height;

  if (!parse_string(ww, &name))
    return 0;
  
  if (!parse_int(ww, &width))
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!new_pixmap(ww, name, width, height))
  {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: CreatePixmap failed: Named pixmap already exists!\n");
    return 0;
  }

  return 1;
}

static int cmd_freepixmap(DrawCanvasWidget ww)
{
  char *name;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!free_pixmap(ww, name))
  {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: FreePixmap failed: Named pixmap not found!\n");
    return 0;
  }

  return 1;
}

static int cmd_freepixmaps(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  free_pixmaps(ww);

  return 1;
}

static int cmd_setdrawtargettopixmap(DrawCanvasWidget ww)
{
  char *name;
  Pixmap p;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!find_pixmap(ww, name, &p, NULL)) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: SetDrawTargetToPixmap failed: Named pixmap not found!\n");
    return 0;
  }
  
  ww->draw.pm = p;
  ww->draw.drawinwindow = 0;

  return 1;
}

static int cmd_setdrawtargettowindow(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  ww->draw.pm = ww->draw.winpm;
  ww->draw.drawinwindow = ww->draw.drawinwindow_set;
  
  return 1;
}

#if 0  /* XXX doesn't work usefully because we need a GC with depth 1 */
static int cmd_setdrawtargettoshapemask(DrawCanvasWidget ww)
{
  char *name;
  Pixmap s;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!find_pixmap(ww, name, NULL, &s)) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: SetDrawTargetToShapemask failed: "
      "Named pixmap not found!\n");
    return 0;
  }
  
  ww->draw.pm = s;
  ww->draw.drawinwindow = 0;

  return 1;
}
#endif

static int cmd_setclipmask(DrawCanvasWidget ww)
{
  char *name;
  Pixmap s;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!find_pixmap(ww, name, NULL, &s)) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: SetClipMask failed: Named pixmap not found!\n");
    return 0;
  }
  
  XSetClipMask(XtDisplay(ww), ww->draw.dgc, s);

  return 1;
}

static int cmd_copyareafrompixmap(DrawCanvasWidget ww)
{
  char *name;
  int src_x, src_y, width, height, dest_x, dest_y;
  Display *disp = XtDisplay(ww);
  Window win = XtWindow(ww);
  Pixmap p;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_int(ww, &src_x))
    return 0;

  if (!parse_int(ww, &src_y))
    return 0;

  if (!parse_int(ww, &width))
    return 0;

  if (!parse_int(ww, &height))
    return 0;

  if (!parse_int(ww, &dest_x))
    return 0;

  if (!parse_int(ww, &dest_y))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!find_pixmap(ww, name, &p, NULL))
  {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: CopyAreaFromPixmap failed: Named pixmap not found!\n");
    return 0;
  }

  XCopyArea(disp, p, ww->draw.pm, ww->draw.dgc,
    src_x, src_y, width, height, dest_x, dest_y);

  if (ww->draw.drawinwindow)
    XCopyArea(disp, p, win, ww->draw.dgc,
      src_x, src_y, width, height, dest_x, dest_y);

  return 1;
}

static int cmd_copyfrompixmap(DrawCanvasWidget ww)
{
  char *name;
  int dest_x, dest_y;
  Pixmap p, s;
  unsigned int width, height;
  Display *disp = XtDisplay(ww);
  Window win = XtWindow(ww);

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_int(ww, &dest_x))
    return 0;

  if (!parse_int(ww, &dest_y))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!find_pixmap(ww, name, &p, &s))
  {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: CopyFromPixmap failed: Named pixmap not found!\n");
    return 0;
  }

  get_pixmap_size(ww, p, &width, &height);

  XSetClipMask(XtDisplay(ww), ww->draw.pmgc, s);
  XSetClipOrigin(XtDisplay(ww), ww->draw.pmgc, dest_x, dest_y);

  XCopyArea(disp, p, ww->draw.pm, ww->draw.pmgc, 0, 0, width, height,
    dest_x, dest_y);

  if (ww->draw.drawinwindow)
    XCopyArea(disp, p, win, ww->draw.pmgc, 0, 0, width, height,
      dest_x, dest_y);

  XSetClipMask(XtDisplay(ww), ww->draw.pmgc, None);

  return 1;
}

static int cmd_getpixmapsize(DrawCanvasWidget ww)
{
  char *name;
  Pixmap p;
  unsigned int width, height;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!find_pixmap(ww, name, &p, NULL)) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: GetPixmapSize failed: Named pixmap not found!\n");
    return 0;
  }

  get_pixmap_size(ww, p, &width, &height);

  returnf(ww, "%d %d", width, height);

  return 1;
}

static int cmd_readfiletopixmap(DrawCanvasWidget ww)
{
  char *filename, *pixmapname;
  Pixmap p, s;
  int ret;

  if (!parse_string(ww, &filename))
    return 0;

  if (!parse_string(ww, &pixmapname))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!ww->draw.allow_file_read) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: ReadFileToPixmap failed: File reading not allowed!\n");
    return 0;
  }

  if (find_pixmap(ww, pixmapname, NULL, NULL)) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: ReadFileToPixmap failed: Named pixmap already exists!\n");
    return 0;
  }

  ret = XpmReadFileToPixmap(XtDisplay(ww),
    RootWindowOfScreen(XtScreen(ww)), filename, &p, &s, NULL);

  if (ret == XpmOpenFailed) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: ReadFileToPixmap failed: File open failed!\n");
    return 0;
  } else if (ret == XpmFileInvalid) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: ReadFileToPixmap failed: Invalid XPM file!\n");
    return 0;
  } else if (ret == XpmNoMemory) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: ReadFileToPixmap failed: Out of memory!\n");
    return 0;
  } else if (ret != XpmSuccess) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: ReadFileToPixmap failed: Unknown XPM error!\n");
    return 0;
  }

  add_pixmap(ww, pixmapname, p, s); /* should always succeed */

  return 1;
}

static int cmd_writefilefrompixmap(DrawCanvasWidget ww)
{
  char *filename, *pixmapname;
  Pixmap p, s;
  int ret;

  if (!parse_string(ww, &filename))
    return 0;

  if (!parse_string(ww, &pixmapname))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!ww->draw.allow_file_write) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromPixmap failed: File writing not allowed!\n");
    return 0;
  }

  if (!find_pixmap(ww, pixmapname, &p, &s)) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromPixmap failed: Named pixmap not found!\n");
    return 0;
  }

  ret = XpmWriteFileFromPixmap(XtDisplay(ww), filename, p, s, NULL);

  if (ret == XpmOpenFailed) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromPixmap failed: File open failed!\n");
    return 0;
  } else if (ret == XpmNoMemory) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromPixmap failed: Out of memory!\n");
    return 0;
  } else if (ret != XpmSuccess) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromPixmap failed: Unknown XPM error!\n");
    return 0;
  }

  return 1;  
}

static int cmd_writefilefromwindow(DrawCanvasWidget ww)
{
  char *filename;
  int ret;

  if (!parse_string(ww, &filename))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (!ww->draw.allow_file_write) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromWindow failed: File writing not allowed!\n");
    return 0;
  }

  ret = XpmWriteFileFromPixmap(XtDisplay(ww), filename, ww->draw.winpm,
    /*XXX*/0, NULL);

  if (ret == XpmOpenFailed) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromWindow failed: File open failed!\n");
    return 0;
  } else if (ret == XpmNoMemory) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromWindow failed: Out of memory!\n");
    return 0;
  } else if (ret != XpmSuccess) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: WriteFileFromWindow failed: Unknown XPM error!\n");
    return 0;
  }

  return 1;  
}

#if 0
/*
 * XXX Half-implemented commands for reading/writing pixmaps from/to
 * strings. Still need to be able to write escaped strings for them to
 * work.
 */
static int cmd_createpixmapfrombuffer(DrawCanvasWidget ww)
{
  char *buffer, *name;
  Pixmap p, s;
  int ret;

  if (!parse_string(ww, &buffer))
    return 0;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  if (find_pixmap(ww, name, NULL, NULL)) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: CreatePixmapFromBuffer failed: "
      "Named pixmap already exists!\n");
    return 0;
  }

  ret = XpmCreatePixmapFromBuffer(XtDisplay(ww),
    RootWindowOfScreen(XtScreen(ww)), buffer, &p, &s, NULL);

  if (ret == XpmFileInvalid) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: CreatePixmapFromBuffer failed: Invalid XPM buffer!\n");
    return 0;
  } else if (ret == XpmNoMemory) { 
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: CreatePixmapFromBuffer failed: Out of memory!\n");
    return 0;
  } else if (ret != XpmSuccess) {
    XtAppWarning(XtWidgetToApplicationContext((Widget) ww),
      "DrawCanvas: CreatePixmapFromBuffer failed: Unknown XPM error!\n");
    return 0;
  }

  add_pixmap(ww, name, p, s); /* should always succeed */

  return 1;
}

static int cmd_createbufferfrompixmap(DrawCanvasWidget ww)
{
  char *name;
  Pixmap p;
  int ret;

  if (!parse_string(ww, &name))
    return 0;

  if (!parse_command_end(ww))
    return 0;

  /* XXX */
}
#endif

/*
 * Internal ("undocumented") commands - these should not be used by
 * external applications!
 */

/* Used to report an end-of-file situation to the widget */
static int cmd__endoffile(DrawCanvasWidget ww)
{
  if (!parse_command_end(ww))
    return 0;

  if (!ww->draw.exit_on_event)
    ww->draw.should_exit = 1;

  return 1;
}

/*
 * Tables of the commands
 */

typedef int (*cmd_funcp)(DrawCanvasWidget);

char *cmd_names[] = {
  "Exit", "Reset", "ResetGC",
  "PushGC", "PopGC", "SetWindowDrawing",
  "RedrawWindowArea", "ResizeWindow",

  "SetForeground", "SetBackground", "SetFunction",
  "SetLineAttributes", "SetDashes", "SetFillRule",
  "SetFont", "SetClipRectangles", "SetArcMode",
  "SetClipOrigin", "ClearClipMask",

  "CopyArea", "DrawPoint", "DrawPoints",
  "DrawLine", "DrawLines", "DrawSegments",
  "DrawRectangle", "DrawRectangles", "DrawArc",
  "DrawArcs", "FillRectangle", "FillRectangles",
  "FillPolygon", "FillArc", "FillArcs",
  "DrawString", "DrawImageString",

  "DrawRoundedRectangle", "FillRoundedRectangle",
  "DrawJustifiedString",

  "GetWindowSize", "GetTextExtents", "GetPointerPosition",
  "GetxdrawVersion",

  "ReportExposeEvents", "ReportResizeEvents",
  "ReportKeyEvents", "ReportButtonEvents",
  "ReportEnterLeaveEvents", "ReportFocusChangeEvents",
  "ReportMotionEvents", "ExitOnEvent",

  "CreatePixmap", "FreePixmap", "FreePixmaps",
  "SetDrawTargetToPixmap", "SetDrawTargetToWindow",
  /*"SetDrawTargetToShapemask",*/ "SetClipMask",
  "CopyAreaFromPixmap", "CopyFromPixmap", "GetPixmapSize",

  "ReadFileToPixmap", "WriteFileFromPixmap", "WriteFileFromWindow",

  "EndOfFile"
};

cmd_funcp cmd_funcs[] = {
  cmd_exit, cmd_reset, cmd_resetgc,
  cmd_pushgc, cmd_popgc, cmd_setwindowdrawing,
  cmd_redrawwindowarea, cmd_resizewindow,

  cmd_setforeground, cmd_setbackground, cmd_setfunction,
  cmd_setlineattributes, cmd_setdashes, cmd_setfillrule,
  cmd_setfont, cmd_setcliprectangles, cmd_setarcmode,
  cmd_setcliporigin, cmd_clearclipmask,

  cmd_copyarea, cmd_drawpoint, cmd_drawpoints,
  cmd_drawline, cmd_drawlines, cmd_drawsegments,
  cmd_drawrectangle, cmd_drawrectangles, cmd_drawarc,
  cmd_drawarcs, cmd_fillrectangle, cmd_fillrectangles,
  cmd_fillpolygon, cmd_fillarc, cmd_fillarcs,
  cmd_drawstring, cmd_drawimagestring,

  cmd_drawroundedrectangle, cmd_fillroundedrectangle,
  cmd_drawjustifiedstring,

  cmd_getwindowsize, cmd_gettextextents, cmd_getpointerposition,
  cmd_getxdrawversion,

  cmd_reportexposeevents, cmd_reportresizeevents,
  cmd_reportkeyevents, cmd_reportbuttonevents,
  cmd_reportenterleaveevents, cmd_reportfocuschangeevents,
  cmd_reportmotionevents, cmd_exitonevent,

  cmd_createpixmap, cmd_freepixmap, cmd_freepixmaps,
  cmd_setdrawtargettopixmap, cmd_setdrawtargettowindow,
  /*cmd_setdrawtargettoshapemask,*/ cmd_setclipmask,
  cmd_copyareafrompixmap, cmd_copyfrompixmap, cmd_getpixmapsize,

  cmd_readfiletopixmap, cmd_writefilefrompixmap, cmd_writefilefromwindow,

  cmd__endoffile
};

int num_cmds = 64;
